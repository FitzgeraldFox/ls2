<?php
$col_count  = mt_rand(50, 100);
$numbersArr = [];
for ($i = 0; $i < $col_count; $i++) {
    $numbersArr[] = mt_rand(1, 100);
}
file_put_contents('output.csv', join(',', $numbersArr));
$csvFile     = fopen('output.csv', 'r');
$fileContent = fgetcsv($csvFile);
$sum = 0;
foreach ($fileContent as $num) {
    if ($num % 2 == 0) {
        $sum += $num;
    }
}
echo "Сумма чётных чисел csv файла: $sum";
fclose($csvFile);