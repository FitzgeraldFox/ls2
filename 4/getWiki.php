<?php
if( $curl = curl_init() ) {
    curl_setopt($curl,CURLOPT_URL,'https://en.wikipedia.org/w/api.php?action=query&titles=Main%20Page&prop=revisions&rvprop=content&format=json');
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true); // не по psr-2
    $out = curl_exec($curl);
    $content = json_decode($out, 1);
    echo "Title: {$content['query']['pages']['15580374']['title']}<br>";
    echo "Page Id: {$content['query']['pages']['15580374']['pageid']}";
    curl_close($curl);
}