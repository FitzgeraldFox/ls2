<html>
<head>
    <title>Данные для курьера</title>
    <style>
        .order_items > * {
            display: inline-block;
            padding: 20px;
            border: 1px solid;
            min-width: 200px;
        }

        .order_items li {
            border-bottom: 1px solid;
            list-style-type: none;
        }
    </style>
</head>
<body>
<?php
if (file_exists('data.xml')) {
    $purchaseOrder = simplexml_load_file('data.xml');
    echo "
    <h1>PurchaseOrder</h1>
    <div>
        <h2>PurchaseOrderNumber: {$purchaseOrder['PurchaseOrderNumber']}</h2>
        <h2>OrderDate: {$purchaseOrder['OrderDate']}</h2>
    </div>
    <h2>Order Adresses</h2>
    <div class='order_items'>
";
    foreach ($purchaseOrder->Address as $address) {
        echo "
        <ul>
            <li><p><b>Address Type:</b> {$address['Type']}</p></li>
            <li><p><b>Name:</b> $address->Name</p></li>
            <li><p><b>Street:</b> $address->Street</p></li>
            <li><p><b>City:</b> $address->City</p></li>
            <li><p><b>State:</b> $address->State</p></li>
            <li><p><b>Zip:</b> $address->Zip</p></li>
            <li><p><b>Country:</b> $address->Country</p></li>
        </ul>
        ";
    }
    echo "</div>
    <h3>Delivery Notes:</h3><p>$purchaseOrder->DeliveryNotes</p>
    <h2>Items</h2>
    <div class='order_items'>
    ";
    foreach ($purchaseOrder->Items->Item as $item) {
        echo "
            <ul>
                <li>
                    <h3>PartNumber: {$item['PartNumber']}</h3>
                </li>
                <li>
                    <p><b>Product Name:</b> $item->ProductName</p>
                </li>
                <li>
                    <p><b>Quantity:</b> $item->Quantity</p>
                </li>
                <li>
                    <p><b>USPrice:</b> $item->USPrice</p>
                </li>";
                if (!empty($item->Comment)) {
                    echo "
                        <li>
                            <p><b>Comment:</b> $item->Comment</p>
                        </li>
                    ";
                }
                if (!empty($item->ShipDate)) {
                    echo "
                        <li>
                            <p><b>Ship Date:</b> $item->ShipDate</p>
                        </li>
                    ";
                }
            echo '</ul>';
    }
    echo '</div>';
} else {
    exit('Не удалось открыть файл data.xml.');
}
?>
</body>
</html>