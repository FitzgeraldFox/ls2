<?php
$json_arr = [
    "Addresses" => [
        "Address1" => [
            "Name"   => "Ellen Adams",
            "Street" => "123 Maple Street",
            "City"   => "Mill Valley"
        ],
        "Address2" => [
            "Name"   => "Tai Yee",
            "Street" => "8 Oak Avenue",
            "City"   => "Old Town"
        ]
    ]
];

file_put_contents('output.json', json_encode($json_arr));

$jsonFileContent = json_decode(file_get_contents('output.json'), 1);
$randInt         = mt_rand(1, 100);
if ($randInt % 2) {
    $addresses = $jsonFileContent['Addresses'];
    $newAddrs = [];
    foreach ($addresses as $key => $addr) {
        $newAddrsItems = [];
        if ($key == 'Address2') {
            continue;
        }
        foreach ($addr as $key2 => $item) {
            if ($key2 == 'Street') {
                continue;
            }
            $newAddrsItems[$key2] = $item;
        }
        $newAddrsItems['Age'] = $randInt;
        $newAddrs[$key]       = $newAddrsItems;
    }
    $newAddrs['SuperAddress'] = [
        'Name'       => 'Andrey',
        'Profession' => "Web-Developer",
        'Hobby'      => 'Learning and Dancing'
    ];
    $addresses = $newAddrs;
    $jsonFileContent['Addresses'] = $addresses;
}

file_put_contents('output2.json', json_encode($jsonFileContent));

$jsonFile1Content = json_decode(file_get_contents('output.json'), 1);
$jsonFile2Content = json_decode(file_get_contents('output2.json'), 1);
$has_diff         = false;
foreach ($jsonFile1Content['Addresses'] as $key => $addr) {
    if (!isset($jsonFile2Content['Addresses'][$key])) {
        echo "- Адрес $key в output2 удалён <br>";
        $has_diff = true;
        continue;
    } else {
        foreach ($addr as $key2 => $addr_attr) {
            if (!isset($jsonFile2Content['Addresses'][$key][$key2])) {
                echo "- Элемент адреса $key/$key2 в output2 удалён <br>";
                $has_diff = true;
                continue;
            }
        }
    }
}
foreach ($jsonFile2Content['Addresses'] as $key => $addr) {
    if (!isset($jsonFile1Content['Addresses'][$key])) {
        echo "+ В output2 добавлен адрес $key <br>";
        $has_diff = true;
        continue;
    } else {
        foreach ($addr as $key2 => $addr_attr) {
            if (!isset($jsonFile1Content['Addresses'][$key][$key2])) {
                echo "+ В output2 добавлен элемент адреса $key/$key2 <br>";
                $has_diff = true;
                continue;
            }
        }
    }
}
if ($has_diff === false) {
    echo 'Файлы output1.json и output2.json идентичны';
}