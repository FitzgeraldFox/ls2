<?php
require './vendor/autoload.php';
require './ClassVk.php';
define("MY_ID", "54676990");

if (!empty($_FILES['file']['name'])) {
    move_uploaded_file($_FILES['file']['tmp_name'], 'public/files/' . $_FILES['file']['name']);
} else {
    die(json_encode(['success' => 'false', 'message' => 'Нет файла для отправки']));
}

if (!empty($_POST['text'])) {
    $message = $_POST['text'];
} else {
    $message = 'Тестовый пост';
}

if (!empty($_POST['user_id'])) {
    $user_id = $_POST['user_id'];
} else {
    $user_id = MY_ID;
}

$api = new VkApi();

$api->vkRequest('photos.getWallUploadServer');//Получние ссылку от сервара для загрузки

$linkForDownload = $api->toArray()['response']['upload_url'];

$api->downloadServer($linkForDownload, 'public/files/' . $_FILES['file']['name']);

//по ссылке от отсылаем запросы
$api->vkRequest('photos.saveWallPhoto', [
  'user_id'     => MY_ID,
  'photo'       => $api->requestDowl->photo,
  'server'      => $api->requestDowl->server,
  'hash'        => $api->requestDowl->hash,
]);
$api->toArray();
$optionsWall        = array(
  'owner_id'      => $user_id,//куда отправляем
  'message'       => $message,//текст
  'attachments'   => $api->ArrayInJson['response'][0]['id'],//что отправить
);

$api->vkRequest('wall.post', $optionsWall);
$result = $api->toArray();
if (is_file('public/files/' . $_FILES['file']['name'])) {
    unlink('public/files/' . $_FILES['file']['name']);
}
if (!empty($result['error'])) {
    die(json_encode(['success' => 'false', 'message' => $result['error']['error_msg']]));
} else {
    $result = $api->ArrayInJson['response'];

    die(json_encode([
      'success' => 'true',
      'message' => "Отправка поста на стену пользователю с id=$user_id прошло успешно. ID поста: {$result['post_id']}"
    ]));
}

