<?php

if (!function_exists('dd')) {
    /**
     * @author Nicolas Grekas <p@tchwork.com>
     */
    function dd($var)
    {
        return dump($var);
    }
}
