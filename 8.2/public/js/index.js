$(document).ready(function () {
   $('body').on('submit', '#socPostForm', function (e) {
       e.preventDefault();
       var form = document.forms.socPostForm;
       var fd = new FormData(form);
       $.ajax({
           url: 'makeWallPost.php',
           type: 'POST',
           data: fd,
           dataType: 'json',
           processData: false,
           contentType: false,
           success: function (response) {
               if (response['success'] == 'true') {
                   new PNotify({
                       title: 'Успех',
                       type: 'success',
                       text: response['message']
                   });
               } else {
                   new PNotify({
                       title: 'Ошибка',
                       type: 'error',
                       text: response['message']
                   });
               }
           }
       })
   })
});