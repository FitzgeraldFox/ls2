<?php
namespace imageOperations;

class FileMagick
{
    public function __construct($start_filepath, $end_filepath, $stamp)
    {
        $mime_type = mime_content_type($start_filepath);
        switch ($mime_type) {
            case 'image/jpeg':
                $image_src   = imagecreatefromjpeg($start_filepath);
                break;
            case 'image/png':
                $image_src   = imagecreatefrompng($start_filepath);
                break;
            case 'image/gif':
                $image_src   = imagecreatefromgif($start_filepath);
                break;
        }
        $image_src_width = imagesx($image_src);
        $image_src_height = imagesy($image_src);
        $max_w = 200;

        if ($image_src_width  >= $max_w) {
            $ratio             = $image_src_width/$max_w;
            $image_dest_width  = round($image_src_width/$ratio);
            $image_dest_height = round($image_src_height/$ratio);
            $image_dest        = imagecreatetruecolor($image_dest_width, $image_dest_height);
            imagecopyresized(
                $image_dest,
                $image_src,
                0,
                0,
                0,
                0,
                $image_dest_width,
                $image_dest_height,
                $image_src_width,
                $image_src_height
            );
        } else {
            $image_dest = imagecreatetruecolor($image_src_width, $image_src_height);
        }
        if (is_file($stamp) !== false) {
            $mime_type = mime_content_type($stamp);
            switch ($mime_type) {
                case 'image/jpeg':
                    $stamp_src = imagecreatefromjpeg($stamp);
                    break;
                case 'image/png':
                    $stamp_src = imagecreatefrompng($stamp);
                    break;
                case 'image/gif':
                    $stamp_src = imagecreatefromgif($stamp);
                    break;
            }

            $stamp_src_width = imagesx($stamp_src);
            $stamp_src_height = imagesy($stamp_src);

            if ($stamp_src_width >= $max_w) {
                $ratio  = $stamp_src_width/$max_w;
                $stamp_dest_width = round($stamp_src_width/$ratio);
                $stamp_dest_height = round($stamp_src_height/$ratio);
                $stamp_dest   = imagecreatetruecolor($stamp_dest_width, $stamp_dest_height);
                imagecopyresized(
                    $stamp_dest,
                    $stamp_src,
                    0,
                    0,
                    0,
                    0,
                    $stamp_dest_width,
                    $stamp_dest_height,
                    $stamp_src_width,
                    $stamp_src_height
                );
            } else {
                $stamp_dest = imagecreatetruecolor($stamp_src_width, $stamp_src_height);
            }

            $margin_right = 0;
            $margin_bottom = 0;
            $sx = imagesx($stamp_dest);
            $sy = imagesy($stamp_dest);

            imagecopymerge(
                $image_dest,
                $stamp_dest,
                imagesx($image_dest) - $sx - $margin_right,
                imagesy($image_dest) - $sy - $margin_bottom,
                0,
                0,
                imagesx($stamp_dest),
                imagesy($stamp_dest),
                50
            );
        }
        $image_dest = imagerotate($image_dest, 45, 0);

        switch ($mime_type) {
            case 'image/jpeg':
                imagejpeg($image_dest, $end_filepath, 9);
                break;
            case 'image/png':
                imagepng($image_dest, $end_filepath, 9);
                break;
            case 'image/gif':
                imagegif($image_dest, $end_filepath, 9);
                break;
        }
        header('Content-Type: image/png');
        readfile($end_filepath);
    }
};

$new = new FileMagick('input.jpg', 'output.jpg', 'stamp.jpg');
