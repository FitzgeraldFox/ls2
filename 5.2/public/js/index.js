$(document).ready(function () {
    $('body').on('submit', '#authForm', function(e){
        e.preventDefault();
        var userData = '';
        switch($(this).attr('data-form-type')) {
            case 'auth':
                userData = $(this).serialize();
                userData += '&action=auth_user';
                $.ajax({
                    url: 'actions.php',
                    type: 'POST',
                    data: userData,
                    dataType: 'json',
                    success: function (response) {
                        if(response['success'] == 'true'){
                            window.location.pathname = response['message'];
                        } else {
                            new PNotify({
                                title: 'Ошибка',
                                type: 'error',
                                text: response['message']
                            })
                        }
                    }
                });
                break;
            case 'registration':
                if($(this).find('input[name=password]').val() != $(this).find('input[name=confirm-password]').val()) {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: 'Введите правильное подтверждение пароля'
                    })
                } else {
                    userData = $(this).serialize();
                    userData += '&action=register_user';
                        $.ajax({
                        url: 'actions.php',
                        type: 'POST',
                        data: userData,
                        dataType: 'json',
                        success: function (response) {
                            if(response['success'] == 'true'){
                                new PNotify({
                                    title: 'Успех',
                                    type: 'success',
                                    text: response['mail_message']
                                });
                                window.location.pathname = response['message'];
                            } else {
                                new PNotify({
                                    title: 'Ошибка',
                                    type: 'error',
                                    text: response['message']
                                })
                            }
                        }
                    });
                }
                break;
        }
    });

    $('body').on('click', '#addUser', function () {
        $('#addUserForm').show();
        $('#addUserForm').arcticmodal({
            afterClose: function(data, el) {
                $('#addUserForm').hide();
                location.reload();
            }
        });
    });

    $('body').on('submit', '#addUserForm', function (e) {
        e.preventDefault();
        if($(this).find('input[name=password]').val() != $(this).find('input[name=confirm-password]').val()) {
            new PNotify({
                title: 'Ошибка',
                type: 'error',
                text: 'Введите правильное подтверждение пароля'
            })
        } else {
            userData = $(this).serialize();
            userData += '&action=register_user';
            $.ajax({
                url: 'actions.php',
                type: 'POST',
                data: userData,
                dataType: 'json',
                success: function (response) {
                    if(response['success'] == 'true'){
                        new PNotify({
                            title: 'Успех',
                            type: 'success',
                            text: response['mail_message']
                        });
                        $('#addUserForm')[0].reset();
                    } else {
                        new PNotify({
                            title: 'Ошибка',
                            type: 'error',
                            text: response['message']
                        })
                    }
                }
            });
        }
    });

    $('body').on('submit', '#userInfoForm', function(e) {
        e.preventDefault();
        var form = document.forms.userInfoForm;
        var fd = new FormData(form);
        fd.append('action', 'update_user');
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: fd,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (response) {
                if(response['success'] == 'true'){
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $.ajax({
                        url: 'actions.php',
                        type: 'POST',
                        data: {'action': 'get_file'},
                        dataType: 'json',
                        success: function (response) {
                            if (response['success'] == 'true') {
                                $('#userFileTable tbody').html(response['message']);
                                $('#userFileTable').fadeIn();
                            }
                        },
                        error: function() {
                            new PNotify({
                                title: 'Ошибка',
                                type: 'error',
                                text: 'Ошибка сервера'
                            })
                        }
                    })
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        });
    });

    $('body').on('click', '#exitLK', function(){
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: {'action': 'exit_user'},
            dataType: 'json',
            success: function(response){
                if (response['redirect'] == 'true') {
                    window.location.pathname = response['auth_page'];
                }
            }
        })
    });

    $('body').on('click', '[data-delete-file]', function(e){
        e.preventDefault();
        var self = $(this);
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: {
                'filename': $(this).closest('tr').find('td:first').html(),
                'action': 'delete_file',
                'uid': $(this).closest('tr').attr('data-user-id')
            },
            dataType: 'json',
            success: function(response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    self.closest('tr').fadeOut(200);
                    setTimeout(function(){
                        self.closest('tr').remove();
                        if($('#userFileTable tbody tr').length == 0) {
                            $('#userFileTable').fadeOut(200);
                            setTimeout(function(){
                                $('#userFileTable').hide();
                            }, 200);
                        }
                    }, 200);
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('click', '[data-edit-user]', function(){
        $('#editUserForm').show();
        var tr = $(this).closest('tr');
        $('#editUserForm [name="email"]').val($(tr).find('td:nth-child(1)').text());
        $('#editUserForm [name="username"]').val($(tr).find('td:nth-child(2)').text());
        $('#editUserForm [name="age"]').val($(tr).find('td:nth-child(3)').text());
        $('#editUserForm [name="about"]').val($(tr).find('td:nth-child(4)').text());
        $('#editUserForm').arcticmodal({
            beforeClose: function(data, el) {
                $.ajax({
                    url: 'actions.php',
                    dataType: 'json',
                    data: {'action': 'updateUserlist_user'},
                    type: 'POST',
                    success: function (response) {
                        if (response['success'] == 'true') {
                            $('#userListTable tbody').html(response['message']);
                        } else {
                            location.reload();
                        }
                    }
                })
            },
            afterClose: function (data, el) {
                $('#editUserForm').hide();
                $('#editUserForm')[0].reset();
            }
        });
    });

    $('body').on('submit', '#editUserForm', function (e) {
        e.preventDefault();
        var form = document.forms.editUserForm;
        var fd = new FormData(form);
        fd.append('action', 'update_user');
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: fd,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $('#editUserForm').arcticmodal('close');
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    });
                }
            }
        });
    });

    $('body').on('click', '[data-delete-user]', function(){
        var self = $(this);
        $.ajax({
            url: 'actions.php',
            type: 'POST',
            data: {'id': self.closest('tr').attr('data-user-id'), 'action': 'delete_user'},
            dataType: 'json',
            success: function(response) {
                if (response['redirect'] == 'true') {
                    window.location.pathname = response['auth_page'];
                }
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    self.closest('tr').fadeOut(200);
                    setTimeout(function(){
                        self.closest('tr').remove();
                        if($('#userListTable tbody tr').length == 0) {
                            $('#userListTable').fadeOut(200);
                            setTimeout(function(){
                                $('#userListTable').hide();
                            }, 200);
                        }
                    }, 200);
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    })
});

