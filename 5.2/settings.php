<?php
function getSettings() {
    return [
      'DRIVER'                                 => 'mysql',
      'HOST'                                   => 'localhost',
      'DATABASE'                               => 'ls3',
      'USERNAME'                               => 'root',
      'PASSWORD'                               => '',
      'CHARSET'                                => 'utf8',
      'COLLATION'                              => 'utf8_unicode_ci',
      'PREFIX'                                 => '',
      'SALT'                                   => '68kENTUCKYlANGUAGEnORTHERNdAMASCUScOMPOUNDdELAWARE01',
      'AUTH_PAGE'                              => '/',
      'UPDATE_USER'                            => 'profile',
      'SUCCESS_REG_MAIL'                       => 'Приветственное письмо послано вам на почту!',
      'REG_ERROR'                              => 'Пользователь с таким Email уже существует!',
      'SIMPLE_REG_ERROR'                       => 'Ошибка при регистрации учётной записи',
      'AUTH_ERROR'                             => 'Неверные имя пользователя или пароль',
      'SIMPLE_AUTH_ERROR'                      => 'Ошибка авторизации',
      'FILTER_ERROR'                           => 'Переданы не все необходимые данные либо данные некорректны',
      'USER_CREATE_ERROR'                      => 'Ошибка при создании пользователя',
      'USER_UPDATE_ERROR'                      => 'Ошибка при обновлении данных',
      'USER_UPDATE_SUCCESS'                    => 'Данные успешно обновлены',
      'WRONG_FILE_EXT'                         => 'Данные обновлены, но файл не загружен, т.к. он не является картинкой',
      'USER_FILE_DELETE_SUCCESS'               => 'Фотография успешно удалена',
      'USER_DELETE_SUCCESS'                    => 'Пользователь успешно удалён',
      'USER_DELETE_ERROR'                      => 'Такого пользователя не существует',
      'LARGE_FILE'                             => 'Загружаемый файл должен быть < 40мб',
      'SMALL_USERNAME'                         => 'Имя пользоавтеля должно быть не менее 5 символов',
      'SMALL_ABOUT'                            => 'Описание должно быть не менее 50 символов',
      'AGE_ERROR'                              => 'Введите свой возраст (целое число от 10 до 100)',
      'EMAIL_REG_ERROR'                        => 'Введите корректный email - адрес',
      'PASS_CONFIRM_ERROR'                     => 'Введите правильное подтверждение пароля',
      'IP_REG_ERROR'                           => 'Некорректный IP-адрес',
      'USER_UPDATE_ERROR_USER_NOT_FOUND'       => 'Пользователь с таким Email не найден',
      'WRONG_ACTION'                           => 'Требуемого вами действия не существует',
      'AUTH_ERROR_USER_NOT_FOUND'              => 'Такой учётной записи пользователя не существует. Проверьте правильность ввода имени пользователя и пароля',
      'RE_CAPTHCHA_ERROR_WRONG'                => 'Вы не прошли проверку Recaptcha',
      'RE_CAPTHCHA_ERROR'                      => 'Ошибка Recaptcha',
      'FILTER_EMAIL_ERROR'                     => 'Неправильный Email - адрес',
      'FILTER_PASSWORD_ERROR'                  => 'В пароле разрешено использовать только следующие символы: a-z, A-Z, 0-9, _-',
      'FILTER_CONFIRM_PASSWORD_ERROR'          => 'Требуется подтверждение пароля. Разрешено использовать только следующие символы: a-z, A-Z, 0-9, _-',
      'FILTER_UPDATE_USERNAME_ERROR'           => 'Неправильное имя пользователя. Минимальная длина - 5 символов. Разрешено использовать только символы: a-z, A-Z, 0-9',
      'FILTER_UPDATE_AGE_ERROR'                => 'Неправильный возраст. Нужно целое число от 10 до 100',
      'FILTER_UPDATE_ABOUT_ERROR'              => 'Длина описания должна быть больше 50 символов',
      'FILTER_ERROR_FILE_WRONG_UID'            => 'Передан неправильный id пользователя',
      'FILTER_UPDATE_PASSWORD_ERROR'           => 'Неправильный пароль. Разрешено использовать только следующие символы: a-z, A-Z, 0-9, _-'
    ];
};
