<?php

namespace Models;

error_reporting(E_ALL);
require_once 'Model.php';
require_once 'vendor/autoload.php';

class User extends \Illuminate\Database\Eloquent\Model
{
    use Model;
    public $timestamps  = false;
    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'age', 'about', 'filename', 'visitor'];

    public function getUserByEmail($email)
    {
        return User::where('email', '=', $email)->get()->toJson();
    }

    public function authUser($params)
    {
        return User::where([
          'email'    => $params['email'],
          'password' => $params['pass']
        ])->get()->toJson();
    }

    public function updateUser($params)
    {
        if (empty($_FILES['file']['name']) ||
                  $_FILES['file']['name'] == 'NULL') {
            $params['filename'] = null;
        } else {
            $file               = $_FILES['file'];
            $params['filename'] = $file['name'];
        }
        if (empty($params['username'])) {
            $params['username'] = null;
        }
        if (empty($params['age'])) {
            $params['age'] = 0;
        }
        if (empty($params['about'])) {
            $params['about'] = null;
        }

        $query_params = [
          'name'  => $params['username'],
          'age'   => $params['age'],
          'about' => $params['about']
        ];
        if (!empty($params['password']) && $params['password'] != 'NULL') {
            $query_params['password'] = crypt($params['password'], $GLOBALS['settings']['SALT']);
        }
        if (!empty($params['email']) && $params['email'] != 'NULL') {
            $user = User::where('email', '=', $params['email'])->first();
            if (count(json_decode($user, 1)) > 0) {
                $query_params['email'] = $params['email'];
            } else {
                die(json_encode([
                  'success' => 'true',
                  'message' => $GLOBALS['settings']['USER_UPDATE_ERROR_USER_NOT_FOUND']
                ]));
            }
        } else {
            $user = User::where('email', '=', $_SESSION['email'])->first();
        }
        if ($params['filename'] !== null) {
            $ext      = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
            $img_exts = ['jpeg', 'jpg', 'png', 'gif'];

            if (!in_array($ext, $img_exts)) {
                die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['WRONG_FILE_EXT']]));
            } else {
                if ($_FILES['file']['size'] > 320000000) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['LARGE_FILE']
                    ]));
                }
                if (empty($file['tmp_name'])) {
                    $user->update($query_params);
                    die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['USER_UPDATE_SUCCESS']]));
                } else {
                    $filename  = json_decode($user, 1)['id'];
                    $filename .= '.' . $ext;
                    $query_params['filename'] = $filename;
                    $user->update($query_params);

                    if (!is_dir('public/userFiles')) {
                        mkdir('public/userFiles');
                    } else {
                        if (is_file("public/userFiles/$filename")) {
                            unlink("public/userFiles/$filename");
                        }
                    }
                    $this->resizeFile($file, "public/userFiles/$filename");
                    die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['USER_UPDATE_SUCCESS']]));
                }
            }
        } else {
            $user->update($query_params);
            die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['USER_UPDATE_SUCCESS']]));
        }
    }

    public function getUserlist()
    {
        return User::select('*')->get()->toArray();
    }

    public function getFile()
    {
        return User::where('email', '=', $_SESSION['email'])->get()->toArray();
    }

    public function deleteFile($params)
    {
        if (is_file('public/userFiles/' . trim($params['filename']))) {
            unlink('public/userFiles/' . trim($params['filename']));
            User::where('id', '=', $params['uid'])->update(['filename' => null]);
            die(json_encode([
              'success' => 'true',
              'message' => $GLOBALS['settings']['USER_FILE_DELETE_SUCCESS']
            ]));
        }
    }

    public function deleteUser($userData)
    {
        $user          = User::find($userData['id']);
        $userQueryData = json_decode($user, 1);
        if (count($userQueryData) > 0) {
            if (is_file('public/userFiles/' . $userQueryData['filename'])) {
                unlink('public/userFiles/' . $userQueryData['filename']);
            }
            $user->delete();
            if ($userQueryData['email'] == $_SESSION['email']) {
                session_unset();
                die(json_encode([
                    'success'   => 'true',
                    'message'   => $GLOBALS['settings']['USER_DELETE_SUCCESS'],
                    'redirect'  => 'true',
                    'auth_page' => $GLOBALS['settings']['AUTH_PAGE']
                ]));
            } else {
                die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['USER_DELETE_SUCCESS']]));
            }
        } else {
            die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['USER_DELETE_ERROR']]));
        }
    }

    public function getUserData($id = '')
    {
        if (!empty($id)) {
            return json_decode(User::where('id', '=', $id)->get(), 1);
        } else {
            return json_decode(User::where('email', '=', $_SESSION['email'])->get(), 1);
        }
    }

    public function getAllUsersData()
    {
        return User::select('*')->get()->toArray();
    }

    public function getFileList()
    {
        return json_decode(User::select(['id', 'filename'])->whereNotnull('filename')->get(), 1);
    }

    public function resizeFile($file, $filepath)
    {
        switch ($file['type']) {
            case 'image/jpeg':
                $image_src   = imagecreatefromjpeg($file['tmp_name']);
                break;
            case 'image/png':
                $image_src   = imagecreatefrompng($file['tmp_name']);
                break;
            case 'image/gif':
                $image_src   = imagecreatefromgif($file['tmp_name']);
                break;
            default:
                $image_src = false;
        }
        if ($image_src) {
            $image_src_width  = imagesx($image_src);
            $image_src_height = imagesy($image_src);
            $max_w            = 480;
            $max_h            = 480;
            if ($image_src_width  != $max_w ||
                $image_src_height != $max_h
            ) {
                $image_dest   = imagecreatetruecolor($max_w, $max_h);
                imagecopyresized(
                    $image_dest,
                    $image_src,
                    0,
                    0,
                    0,
                    0,
                    $max_w,
                    $max_h,
                    $image_src_width,
                    $image_src_height
                );
                switch ($file['type']) {
                    case 'image/jpeg':
                        imagejpeg($image_dest, $filepath, 100);
                        break;
                    case 'image/png':
                        imagepng($image_dest, $filepath, 100);
                        break;
                    case 'image/gif':
                        imagegif($image_dest, $filepath, 100);
                        break;
                }
            } else {
                move_uploaded_file($file['tmp_name'], $filepath);
            }
        }
    }
}