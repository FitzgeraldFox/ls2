<?php

namespace Models;

require "vendor/autoload.php";
require_once 'settings.php';
use \Illuminate\Database\Capsule\Manager as Capsule;

trait Model
{
    public function __construct()
    {
        $settings = getSettings();
        $capsule = new Capsule;
        $capsule->addConnection([
          'driver'    => $settings['DRIVER'],
          'host'      => $settings['HOST'],
          'database'  => $settings['DATABASE'],
          'username'  => $settings['USERNAME'],
          'password'  => $settings['PASSWORD'],
          'charset'   => $settings['CHARSET'],
          'collation' => $settings['COLLATION'],
          'prefix'    => $settings['PREFIX']
        ]);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}