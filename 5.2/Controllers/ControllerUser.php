<?php

namespace Controllers;

error_reporting(E_ERROR);
require_once 'vendor/autoload.php';
require_once 'Models/User.php';
use Models\User;
use PHPMailer;
use GUMP;

session_start();

class ControllerUser
{

    protected $userData;
    protected $User;

    public function __construct()
    {
        $this->User = new User;
    }

    public function checkAction()
    {
        $userData = $this->filter();
        switch ($_POST['action']) {
            case 'auth_user':
                $this->authUser($userData);
                break;
            case 'register_user':
                $this->registerUser($userData);
                break;
            case 'update_user':
                $this->User->updateUser($userData);
                break;
            case 'updateUserlist_user':
                $users = $this->User->getUserlist();
                $userList  = '';
                foreach ($users as $user) {
                    $userList .= "
                    <tr data-user-id={$user['id']}>
                        <td>{$user['email']}</td>";
                    if (!empty($user['name'])) {
                        $userList .=  "<td>{$user['name']}</td>";
                    } else {
                        $userList .=  '<td>Нет имени</td>';
                    };
                    if (!empty($user['age'])) {
                        $userList .=  "<td>{$user['age']}</td>";
                    } else {
                        $userList .=  '<td>Нет возраста</td>';
                    };
                    if (!empty($user['about'])) {
                        $userList .=  "<td>{$user['about']}</td>";
                    } else {
                        $userList .=  '<td>Нет описания</td>';
                    };
                    if (is_file("public/userFiles/{$user['filename']}")) {
                        $userList .=  "<td><img src=public/userFiles/{$user['filename']}></td>";
                    } else {
                        $userList .=  "<td>Нет аватарки</td>";
                    }
                        $userList .=  "
                        <td>
                            <a href='#' data-edit-user>Редактировать</a>
                            <a href='#' data-delete-user>Удалить</a>
                        </td>
                    </tr>";
                    }

                    die(json_encode(['success' => 'true', 'message' => $userList]));
                break;
            case 'delete_user':
                $this->User->deleteUser($userData);
                break;
            case 'exit_user':
                $this->exitUser();
                break;
            default:
                die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['WRONG_ACTION']]));
        }
    }

    public function authUser($userData)
    {
        $pass = crypt($userData['password'], $GLOBALS['settings']['SALT']);
        $user = $this->User->authUser([
          'email' => $userData['email'],
          'pass'  => $pass
        ]);
        if (count($user) > 0) {
            session_start();
            $_SESSION['email'] = $userData['email'];
            die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['UPDATE_USER']]));
        } else {
            die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['AUTH_ERROR_USER_NOT_FOUND']]));
        }
    }

    public function registerUser($params)
    {
        if (empty($_SESSION['email'])) {
            $this->checkGrecaptcha();
        }
        $user = $this->User->getUserByEmail($params['email']);
        if (count($user) > 0) {
            die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['REG_ERROR']]));
        }
        $pass              = crypt($params['password'], $GLOBALS['settings']['SALT']);
        $_SESSION['email'] = $params['email'];
        $user              = new User;
        $user->email       = $params['email'];
        $user->password    = $pass;
        $user->visitor     = $_SERVER['REMOTE_ADDR'];
        $user->save();
        $send_reg_mail  = self::sendRegMail($params);
        echo json_encode([
          'success' => 'true',
          'message' => $GLOBALS['settings']['UPDATE_USER'],
          'mail_message' => $send_reg_mail
        ]);
    }

    public function checkGrecaptcha()
    {
        $postData = array(
          'secret' => '6LcgKhsUAAAAAGIQM_CIiSzJ_MRCuWvLjozyYIxX',
          'response' => $_POST['g-recaptcha-response']
        );
        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        if (!curl_errno($ch)) {
            if (json_decode($response, true)['success'] == false) {
                die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['RE_CAPTHCHA_ERROR_WRONG']]));
            }
        } else {
            die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['RE_CAPTHCHA_ERROR']]));
        }
        curl_close($ch);
    }

    public static function sendRegMail($userData)
    {
        $smtpConfig = [
          'host'      => 'smtp.yandex.ru',
          'username'  => 'cool-testmail.test@yandex.ru',
          'password'  => '2oR8YvYrCrS6',
          'port'      => '25'
        ];

        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->Host = $smtpConfig['host'];
        $mail->setLanguage("ru");
        if (!empty($smtpConfig['username'])) {
            $mail->SMTPAuth = true;
            $mail->Username = $smtpConfig['username'];
            $mail->Password = $smtpConfig['password'];
        } else {
            $mail->SMTPAuth = false;
        }
        if (isset($smtpConfig['port'])) {
            $mail->Port = $smtpConfig['port'];
        }
        if (isset($smtpConfig['secure'])) {
            $mail->SMTPSecure = $smtpConfig['secure'];
        }

        $mail->CharSet    = "UTF-8";
        $mail->From       = 'cool-testmail.test@yandex.ru';
        $mail->FromName   = 'Мегасайт!';
        $mail->AddAddress($userData['email']);
        $mail->Subject    = 'Регистрация на сайте';
        $body             = "<h1>Вы успешно зарегистрировались</h1>";
        $mail->MsgHTML($body);
        $mail->AltBody    = 'Вы успешно зарегистрировались.';

        try {
            if (!$mail->Send()) {
                return 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                return $GLOBALS['settings']['SUCCESS_REG_MAIL'];
            }
        } catch (\phpmailerException $e) {
            return $e->getMessage();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function filter()
    {
        $gump = new GUMP;
        switch ($_POST['action']) {
            case 'auth_user':
                $gump->validation_rules([
                  'email' => 'required|valid_email'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_EMAIL_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_EMAIL_ERROR']
                    ]));
                }
                $gump->validation_rules([
                  'password' => 'required|alpha_dash'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_PASSWORD_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_PASSWORD_ERROR']
                    ]));
                }
                $gump->filter_rules([
                  'email'    => 'trim|sanitize_email',
                  'password' => 'trim'
                ]);
                $validated_data = $gump->run($_POST);
                break;
            case 'register_user':
                $gump->validation_rules([
                  'REMOTE_ADDR'    => 'required|valid_ip'
                ]);
                try {
                    if (!$gump->run($_SERVER)) {
                        die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['IP_REG_ERROR']]));
                    }
                } catch (\Exception $e) {
                    die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['IP_REG_ERROR']]));
                }
                $gump->validation_rules([
                  'email' => 'required|valid_email'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_EMAIL_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_EMAIL_ERROR']
                    ]));
                }
                $gump->validation_rules([
                  'password' => 'required|alpha_dash'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_PASSWORD_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_PASSWORD_ERROR']
                    ]));
                }
                $gump->validation_rules([
                  'confirm-password' => 'required|alpha_dash'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_CONFIRM_PASSWORD_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_CONFIRM_PASSWORD_ERROR']
                    ]));
                }

                $gump->filter_rules([
                  'email'            => 'trim|sanitize_email',
                  'password'         => 'trim',
                  'confirm-password' => 'trim'
                ]);
                $validated_data = $gump->run($_POST);

                if ($validated_data['password'] != $validated_data['confirm-password']) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['PASS_CONFIRM_ERROR']
                    ]));
                }
                break;
            case 'update_user':
                $gump->validation_rules([
                  'username' => 'required|alpha_numeric|min_len,5'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_UPDATE_USERNAME_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_UPDATE_USERNAME_ERROR']
                    ]));
                }
                if (isset($_POST['password']) && !empty($_POST['password'])) {
                    $gump->validation_rules([
                      'password' => 'required|alpha_dash'
                    ]);
                    try {
                        if (!$gump->run($_POST)) {
                            die(json_encode([
                              'success' => 'false',
                              'message' => $GLOBALS['settings']['FILTER_UPDATE_PASSWORD_ERROR']
                            ]));
                        }
                    } catch (\Exception $e) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_UPDATE_USERNAME_ERROR']
                        ]));
                    }
                }
                $gump->validation_rules([
                  'age' => 'required|integer|min_numeric,10|max_numeric,100'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_UPDATE_AGE_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_UPDATE_AGE_ERROR']
                    ]));
                }
                $gump->validation_rules([
                  'about' => 'required|min_len,50'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_UPDATE_ABOUT_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_UPDATE_ABOUT_ERROR']
                    ]));
                }
                $gump->filter_rules([
                  'username' => 'trim|sanitize_string',
                  'age'      => 'trim|sanitize_numbers',
                  'about'    => 'trim|sanitize_string'
                ]);
                $validated_data = $gump->run($_POST);
                break;
            case 'delete_user':
                $gump->validation_rules([
                  'id' => 'required|integer'
                ]);
                try {
                    if (!$gump->run($_POST)) {
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['FILTER_UPDATE_ABOUT_ERROR']
                        ]));
                    }
                } catch (\Exception $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_UPDATE_ABOUT_ERROR']
                    ]));
                }
                $gump->filter_rules([
                  'id' => 'trim|sanitize_numbers'
                ]);
                $validated_data = $gump->run($_POST);
                break;
            case 'exit_user':
            case 'updateUserlist_user':
                return '';
                break;
            default:
                die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['WRONG_ACTION']]));
        }
        return $validated_data;
    }

    public function exitUser()
    {
        if (isset($_SESSION['email'])) {
            unset($_SESSION["email"]);
        }
        die(json_encode(['redirect' => 'true', 'auth_page' => $GLOBALS['settings']['AUTH_PAGE']]));
    }
}