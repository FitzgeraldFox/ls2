<?php
namespace Controllers;

error_reporting(E_ERROR);
require_once 'vendor/autoload.php';
require_once 'Models/User.php';
use Models\User;
use GUMP;

session_start();

class ControllerFile
{

    protected $userData;
    protected $User;

    public function __construct()
    {
        $this->User = new User;
    }

    public function checkAction()
    {
        $userData = $this->filter();
        switch ($userData['action']) {
            case 'get_file':
                $user = $this->User->getFile();
                if (count($user) > 0 && $user[0]['filename'] != null) {
                    die(json_encode([
                      "success" => 'true',
                      "message" => "
                        <tr data-user-id={$user[0]['id']}>
                            <td>
                                {$user[0]['filename']}
                            </td>
                            <td>
                                <img src='public/userFiles/{$user[0]['filename']}'>
                            </td>
                            <td>
                                <a href=# data-delete-file>Удалить аватарку пользователя</a>
                            </td>
                        </tr>"]));
                } else {
                    die(json_encode(['success' => 'false']));
                }
                break;
            case 'delete_file':
                $this->User->deleteFile($userData);
                break;
            default:
                die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['WRONG_ACTION']]));
        }
    }

    public function filter()
    {
        $gump = new GUMP();
        switch ($_POST['action']) {
            case 'get_file':
                $validated_data = $gump->run($_POST);
                break;
            case 'delete_file':
                $gump->validation_rules([
                  'uid'      => 'required|integer'
                ]);
                $gump->filter_rules([
                  'uid'      => 'trim|sanitize_numbers'
                ]);
                $validated_data = $gump->run($_POST);

                if ($validated_data === false) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['FILTER_ERROR_FILE_WRONG_UID']
                    ]));
                }
                break;
        }
        return $validated_data;
    }
}
