<?php

namespace Controllers;

require_once 'ControllerUser.php';
require_once 'Views/View.php';
require_once 'vendor/autoload.php';
require_once 'Models/User.php';
use Models\User;
use Views\View;

class ControllerPage
{
    public static function render()
    {
        $view = new View;
        $user = new User;
        switch ($_SERVER['REQUEST_URI']) {
            case '/':
                if (isset($_SESSION['email'])) {
                    header('Location: ' . $GLOBALS['settings']['UPDATE_USER']);
                }
                $view->generate('auth.twig');
                break;
            case '/auth':
                if (isset($_SESSION['email'])) {
                    header('Location: ' . $GLOBALS['settings']['UPDATE_USER']);
                }
                $view->generate('auth.twig');
                break;
            case '/register':
                if (isset($_SESSION['email'])) {
                    header('Location: ' . $GLOBALS['settings']['UPDATE_USER']);
                }
                $view->generate('register.twig');
                break;
            case '/profile':
                if (!isset($_SESSION['email'])) {
                    header('Location: ' . $GLOBALS['settings']['AUTH_PAGE']);
                }
                $userData = $user->getUserData();
                if ($userData) {
                    if (!is_file('public/userFiles/' . $userData[0]['filename'])) {
                        $userData['userFileTableStyle'] = 'style="display: none"';
                    } else {
                        $userData['fileTblRow'] = "<tr data-user-id={$userData[0]['id']}>
                    <td>{$userData[0]['filename']}</td>
                    <td><img src=public/userFiles/{$userData[0]['filename']} alt=''></td>
                    <td>
                        <a href='#' data-delete-file>Удалить аватарку пользователя</a>
                    </td>
                </tr>";
                    }
                } else {
                    $userData['userFileTableStyle'] = "style='display: none'";
                }
                $userData['name']  = (
                  !empty($userData[0]['name']) &&
                  $userData[0]['name'] != 'NULL') ?
                  $userData[0]['name'] : '';
                $userData['age']   = (
                  !empty($userData[0]['age']) &&
                  $userData[0]['age'] != 'NULL') ?
                  $userData[0]['age'] : '';
                $userData['about'] = (
                  !empty($userData[0]['about']) &&
                  $userData[0]['about'] != 'NULL') ?
                  $userData[0]['about'] : '';
                if (!empty($userData[0]['age'])) {
                    if ($userData[0]['age'] > 18) {
                        $userData['is_adult'] = '(Совершеннолетний)';
                    } else {
                        $userData['is_adult'] = '(Несовершеннолетний)';
                    }
                } else {
                    $userData['is_adult'] = '(Возраст не указан)';
                }
                $view->generate('profile.twig', $userData);
                break;
            case '/userlist':
                if (!isset($_SESSION['email'])) {
                    header('Location: ' . $GLOBALS['settings']['AUTH_PAGE']);
                }
                $users = $user->getAllUsersData();
                $userList  = '';
                foreach ($users as $user) {
                    $userList .= "
                <tr data-user-id={$user['id']}>
                    <td>{$user['email']}</td>";
                    if (!empty($user['name'])) {
                        $userList .=  "<td>{$user['name']}</td>";
                    } else {
                        $userList .=  '<td>Нет имени</td>';
                    };
                    if (!empty($user['age'])) {
                        $userList .=  "<td>{$user['age']}</td>";
                    } else {
                        $userList .=  '<td>Нет возраста</td>';
                    };
                    if (!empty($user['about'])) {
                        $userList .=  "<td>{$user['about']}</td>";
                    } else {
                        $userList .=  '<td>Нет описания</td>';
                    };
                    if (is_file("public/userFiles/{$user['filename']}")) {
                        $userList .=  "<td><img src=public/userFiles/{$user['filename']}></td>";
                    } else {
                        $userList .=  "<td>Нет аватарки</td>";
                    }
                    $userList .=  "
                    <td>
                        <a href='#' data-edit-user>Редактировать</a>
                        <a href='#' data-delete-user>Удалить</a>
                    </td>
                </tr>";
                }
                $userData = [];
                $userData['userList'] = $userList;
                $userData['li_class_1'] = 'class=active';
                $view->generate('userlist.twig', $userData);
                break;
            case '/filelist':
                if (!isset($_SESSION['email'])) {
                    header('Location: ' . $GLOBALS['settings']['AUTH_PAGE']);
                }
                $filenames = $user->getFileList();
                $fileList = '';
                foreach ($filenames as $file) {
                    if (is_file("public/userFiles/{$file['filename']}")) {
                        $fileList .= "
                            <tr data-user-id={$file['id']}>
                                <td>{$file['filename']}</td>
                                <td><img src=public/userFiles/{$file['filename']}></td>
                                <td><a href='#' data-delete-file>Удалить аватарку пользователя</a></td>
                            </tr>";
                    };
                }
                if (empty($fileList)) {
                    $fileList = '<tr><td colspan="3">Список файлов пользователей пуст</td></tr>';
                }
                $view->generate('filelist.twig', [
                  'fileList' => $fileList,
                  'li_class_2' => 'class=active'
                ]);
                break;
        }
    }
}