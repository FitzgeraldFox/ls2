<?php
require_once 'Controllers/ControllerUser.php';
use Controllers\ControllerUser;
require_once 'Controllers/ControllerFile.php';
use Controllers\ControllerFile;

require_once 'settings.php';
$GLOBALS['settings'] = getSettings();
switch (explode('_', $_POST['action'])[1]) {
    case 'user':
        $controllerUser = new ControllerUser;
        $controllerUser->checkAction();
        break;
    case 'file':
        $controllerFile = new ControllerFile;
        $controllerFile->checkAction();
        break;
    default:
        die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['FILTER_ERROR']]));
}
