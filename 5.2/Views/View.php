<?php
namespace Views;

error_reporting(-1);
require_once 'vendor/autoload.php';
use Twig_Loader_Filesystem;
use Twig_Environment;

class View
{
    private $twig;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(dirname(__DIR__) . '/Views/twig');

        $twig   = new Twig_Environment($loader, [
            'cache' => false
          ]);
        $this->twig = $twig;
    }

    function generate($content_view, $data = []){
        echo $this->twig->render($content_view, $data);
    }

}