<?php
require_once 'Controllers/ControllerPage.php';
require_once 'settings.php';
use Controllers\ControllerPage;

$GLOBALS['settings'] = getSettings();
ControllerPage::render();
