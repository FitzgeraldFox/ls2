<?php
namespace Car;

require_once 'Interfaces/Car.php';

class Lambo extends Car
{
    public function run()
    {
        if (!empty($this->distance) &&
          !empty($this->speed) &&
          !empty($this->direction)
        ) {
            $this->auto_parts['engine']->on();
            $this->auto_parts['transmission']->setTransmission([
              'direction' => $this->direction,
              'speed'     => $this->speed
            ]);
            if ($this->direction == 'forward') {
                $path_str = 'вперёд';
            } else {
                $path_str = 'назад';
            }

            echo 'А это клёвая Ламбо и у неё свой метод run! Движение ' . $path_str . ' началось.<br>';

            $moved_distance = $this->distance;
            $sec = 0;
            while ($moved_distance > 0) {
                if ($this->auto_parts['engine']->getTemperature() >= $this->max_temperature) {
                    echo "Температура двигателя >= {$this->max_temperature} градусов 
                    ({$this->auto_parts['engine']->getTemperature()}). Его надо охладить.<br>";
                    $this->auto_parts['engine']->runCooler();
                } else {
                    $moved_distance -= $this->speed;
                    $this->auto_parts['engine']->plusTemperature($this->speed);
                    echo "Температура двигателя Ламбо: {$this->auto_parts['engine']->getTemperature()}<br>";
                    $diff = $this->distance - $moved_distance;
                    echo 'Ламбо проехала: ' . $diff . ' м.<br>';
                    echo "Ламбо осталось проехать: $moved_distance м.<br>";
                }
                $sec++;
            }
            $this->auto_parts['transmission']->stop();
            $this->auto_parts['engine']->off();
            echo "Поездка завершена! Ламбо превосходна! Было проехано $this->distance м. 
            за $sec с. в направлении '$path_str' со скоростью $this->speed м/с.<br>";
        } else {
            echo 'Параметры движения (расстояние, скорость, направление) не установлены.<br>';
        }
    }
};
