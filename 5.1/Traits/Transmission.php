<?php

namespace Traits;

trait Transmission
{
    public function moveBack($params)
    {
        if ($params['speed'] > 20) {
            $this->curr_trans = 'backward-2';
            echo 'Включена вторая задняя передача, т.к. скорость больше 20 м/с.<br>';
        } elseif ($params['speed'] > 0 && $params['speed'] < 20) {
            $this->curr_trans = 'backward-1';
            echo 'Включена первая задняя передача, т.к. скорость больше 0 и меньше 20 м/с.<br>';
        }
    }
}
