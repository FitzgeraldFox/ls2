<?php

namespace Car;

require_once 'Interfaces/Car.php';
require_once 'Interfaces/Engine.php';
require_once 'Interfaces/Transmission.php';
use Interfaces\ICar;
use Interfaces\IEngine;
use Interfaces\ITransmission;

class Car implements ICar
{
    protected $distance;
    protected $speed;
    protected $direction;
    protected $auto_parts = [];
    protected $max_temperature;

    public function __construct(IEngine $engine, ITransmission $transmission, $max_temperature = 90)
    {
        $this->auto_parts['engine'] = $engine;
        $this->auto_parts['transmission'] = $transmission;
        $this->max_temperature = $max_temperature;
    }

    public function setRunParams($distance, $speed, $direction)
    {
        $err_flag = false;

        if ($distance < 0) {
            echo 'Расстояние должно быть положительным<br>';
            $err_flag = true;
        }

        if ($speed > ($this->auto_parts['engine']->getPower() * 2)) {
            echo 'Указанная скорость превышает максимальную скорость двигателя<br>';
            $err_flag = true;
        }

        if ($direction != 'forward' && $direction != 'backward') {
            echo 'Неправильное направление<br>';
            $err_flag = true;
        }

        if ($err_flag === false) {
            $this->distance  = $distance;
            $this->speed     = $speed;
            $this->direction = $direction;
            echo 'Параметры движения установлены<br>';
        }
    }

    public function run()
    {

        if (!empty($this->distance) &&
            !empty($this->speed) &&
            !empty($this->direction)
        ) {
            $this->auto_parts['engine']->on();

            $this->auto_parts['transmission']->setTransmission([
              'direction' => $this->direction,
              'speed'     => $this->speed
            ]);
            if ($this->direction == 'forward') {
                $path_str = 'вперёд';
            } else {
                $path_str = 'назад';
            }
            echo "Движение $path_str началось.<br>";

            $moved_distance = $this->distance;
            $sec = 0;

            while ($moved_distance > 0) {
                if ($this->auto_parts['engine']->getTemperature() >= $this->max_temperature) {
                    echo "Температура двигателя >= $this->max_temperature 
                    градусов ({$this->auto_parts['engine']->getTemperature()}). Его надо охладить.<br>";
                    $this->auto_parts['engine']->runCooler();
                } else {
                    $moved_distance -= $this->speed;
                    $this->auto_parts['engine']->plusTemperature($this->speed);
                    echo "Температура двигателя: {$this->auto_parts['engine']->getTemperature()} градусов<br>";
                    $diff = $this->distance - $moved_distance;
                    echo 'Машина проехала: ' . $diff . ' м.<br>';
                    echo "Осталось проехать: $moved_distance м.<br>";
                }
                $sec++;
            }
            echo "Поездка завершена! Было проехано $this->distance м. 
            за $sec с. в направлении '$path_str' со скоростью $this->speed м/с.<br>";
            $this->auto_parts['transmission']->stop();
            $this->auto_parts['engine']->off();
        } else {
            echo 'Параметры движения (расстояние, скорость, направление) не установлены.<br>';
        }
    }
}
