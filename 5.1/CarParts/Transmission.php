<?php
namespace CarParts;

require_once 'Interfaces/Transmission.php';
require_once 'Traits/Transmission.php';
use Interfaces\ITransmission;

class Transmission implements ITransmission
{
    use \Traits\Transmission;
    protected $mode;
    protected $trans = ['forward', 'backward', 'neutral'];
    protected $curr_trans = 'neutral';

    public function __construct($transmission)
    {
        switch ($transmission) {
            case 'auto':
                $this->mode = 'auto';
                break;
            case 'manual':
                $this->mode = 'manual';
                break;
        }
    }

    public function setTransmission($params)
    {
        switch ($this->mode) {
            case 'manual':
                switch ($params['direction']) {
                    case 'forward':
                        if ($params['speed'] > 20) {
                            $this->curr_trans = 'forward-2';
                            echo 'Включена вторая передняя передача, т.к. скорость больше 20 м/с.<br>';
                        } elseif ($params['speed'] > 0 && $params['speed'] <= 20) {
                            $this->curr_trans = 'forward-1';
                            echo 'Включена первая передняя передача, т.к. скорость больше 0 и меньше 20 м/с.<br>';
                        }
                        break;
                    case 'backward':
                        $this->moveBack($params);
                        break;
                }
                break;
        }
    }

    public function stop()
    {
        $this->curr_trans = 'neutral';
        echo 'Включена нейтральная передача. Машина остановлена<br>';
    }
}
