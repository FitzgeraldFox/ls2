<?php
namespace {
    require_once 'Interfaces/Engine.php';
}
namespace CarParts {
    require_once 'Transmission.php';
    use Interfaces\IEngine;

    class Engine implements IEngine
    {
        protected $mode = 'off';
        protected $temperature = 0;
        protected $horse_powers = 50;

        public function __construct($horse_powers)
        {
            $this->horse_powers = $horse_powers;
        }

        public function on()
        {
            $this->mode = 'on';
            echo 'Двигатель запущен<br>';
        }

        public function off()
        {
            $this->mode = 'off';
            $this->cooler();
            echo 'Двигатель остановлен<br>';
        }

        protected function cooler()
        {
            if ($this->mode == 'off') {
                $this->temperature = 0;
                echo 'Двигатель полностью охлаждён<br>';
            } else {
                $this->mode = 'cool';
                $this->temperature = $this->temperature - 10;
                echo "Текущая температура двигателя: $this->temperature<br>";
                echo 'Двигатель охлаждён на 10 градусов<br>';
                $this->mode = 'on';
            }
        }

        public function runCooler()
        {
            $this->cooler();
        }

        public function getPower()
        {
            return $this->horse_powers;
        }

        public function getTemperature()
        {
            return $this->temperature;
        }

        public function plusTemperature($temperature)
        {
            $this->temperature += $temperature / 2;
        }
    }
}