<?php

namespace CarParts;

class LamboTransmission extends Transmission
{
    public function setTransmission($params)
    {
        switch ($this->mode) {
            case 'manual':
                switch ($params['direction']) {
                    case 'forward':
                        if ($params['speed'] > 20) {
                            $this->curr_trans = 'forward-2';
                            echo 'Включена вторая передняя передача трансмиссии Ламбо, 
                            т.к. скорость больше 20 м/с.<br>';
                        } elseif ($params['speed'] > 0 && $params['speed'] <= 20) {
                            $this->curr_trans = 'forward-1';
                            echo 'Включена первая передняя передача трансмиссии Ламбо, 
                            т.к. скорость больше 0 и меньше 20 м/с.<br>';
                        }
                        break;
                    case 'backward':
                        $this->moveBack($params);
                        break;
                }
                break;
        }
    }
}
