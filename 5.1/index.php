<?php
require_once 'Car.php';
require_once 'LamboCar.php';
require_once 'CarParts/Engine.php';
require_once 'CarParts/Transmission.php';
require_once 'CarParts/LamboTransmission.php';
use CarParts\Engine;
use CarParts\LamboTransmission;
use Car\Lambo;

$lambo = new Lambo(
    new Engine(20),
    new LamboTransmission('manual')
);
$lambo->setRunParams(500, 30, 'backward');
$lambo->run();
$lambo->setRunParams(1000, 20, 'forward');
$lambo->run();
