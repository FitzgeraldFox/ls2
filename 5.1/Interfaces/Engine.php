<?php
namespace Interfaces;

interface IEngine
{
    public function on();
    public function off();
    public function runCooler();
    public function getPower();
    public function getTemperature();
    public function plusTemperature($temperature);
}
