<?php
namespace Interfaces;

interface ITransmission
{
    public function setTransmission($params);
    public function stop();
}