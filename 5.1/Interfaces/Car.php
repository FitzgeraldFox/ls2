<?php
namespace Interfaces;

interface ICar
{
    public function setRunParams($distance, $speed, $direction);
    public function run();
}

