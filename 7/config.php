<?php
require_once 'settings.php';
require "vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;

$settings = getSettings();
$capsule = new Capsule;
$capsule->addConnection([
  'driver'    => $settings['DRIVER'],
  'host'      => $settings['HOST'],
  'database'  => $settings['DATABASE'],
  'username'  => $settings['USERNAME'],
  'password'  => $settings['PASSWORD'],
  'charset'   => $settings['CHARSET'],
  'collation' => $settings['COLLATION'],
  'prefix'    => $settings['PREFIX']
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
