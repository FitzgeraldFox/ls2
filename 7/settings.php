<?php
function getSettings()
{
    return [
      'DRIVER'                   => 'mysql',
      'HOST'                     => 'localhost',
      'DATABASE'                 => 'ls7',
      'USERNAME'                 => 'root',
      'PASSWORD'                 => '',
      'CHARSET'                  => 'utf8',
      'COLLATION'                => 'utf8_unicode_ci',
      'PREFIX'                   => ''
    ];
}