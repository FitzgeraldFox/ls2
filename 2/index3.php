<?php
function calcEverything()
{
    $args = func_get_args();
    $sign = array_shift($args);

    foreach ($args as $arg) {
        if (!is_numeric($arg)) {
            echo 'Error: Все аргументы после первого должны быть целыми числами<br>';
            return false;
        }
    }

    switch ($sign) {
        case '+':
            echo 'Сумма: ' . array_sum($args) . '<br>';
            break;
        case '-':
            $res = 0;
            foreach ($args as $num) {
                $res = $res - $num;
            }
            echo "Разность (сумма чисел с обратным знаком): $res<br>";
            break;
        case "*":
            $res = $args[0];
            for ($i = 1; $i < count($args); $i++) {
                $res = $res * $args[$i];
            }
            echo "Умножение: $res<br>";
            break;
        default:
            echo 'Error: Неправильный знак операции<br>';
            return false;
    }
}

calcEverything('+', 1, 2, 3, 5.2);
calcEverything('-', 1, 2, 3, 5.2);
calcEverything('*', 1, 2, 3, 5.2);
calcEverything('*', 1, 2, 3, 'asdasd');
calcEverything('asdasd', 1, 2, 3, 5.2);
