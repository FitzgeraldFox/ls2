<?php
// а если ввести не  true а 1?
function array_as_str($arr, $str_mode = false)
{
    $output = '';
    
    if ((boolean) $str_mode) {
        return '<p>' . join($arr, '</p><p>') . '</p>';
    } else {
        foreach ($arr as $str) {
            echo "<p>$str</p>";
        }
    }
}

$arr_of_months = ['june', 'july', 'august'];
echo array_as_str($arr_of_months);
$arr_of_months = ['september', 'october', 'november'];
echo array_as_str($arr_of_months, 1);
