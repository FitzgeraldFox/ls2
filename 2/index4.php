<?php
function getTable($num1 = '', $num2 = '')
{
    if (!is_int($num1) || !is_int($num2)) {
        echo 'Error: Функция принимает 2 целых числа<br>';
        return false;
    }
    echo printTbl($num1, $num2);
}

function printTbl($num1, $num2)
{
    $tbl = '<table><tbody>';

    for ($i = 1; $i < $num1; $i++) {
        $tbl .= '<tr>';
        for ($j = 1; $j < $num2; $j++) {
            $tbl .= '<td>'.$i*$j.'</td>';
        }
        $tbl .= '</tr>';
    }
    $tbl .= '</tbody></table>';
    return $tbl;
}

getTable(15, 15);
getTable(15);
getTable(15, 'dfg');
