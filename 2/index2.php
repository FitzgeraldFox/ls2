<?php
function arr_calc($arr_of_nums, $oper_sign)
{

    if (!is_array($arr_of_nums)) {
        echo 'Error: Первый аргумент должен быть массивом.';
        return false;
    }

    foreach ($arr_of_nums as $num_val) {
        if (!is_numeric($num_val)) {
            echo 'Error: Элементы массива должны быть числами';
            return false;
        }
    }

    switch ($oper_sign) {
        case '+':
            echo 'Сумма: ' . array_sum($arr_of_nums) . '<br>';
            break;
        case '-':
            $result_num = 0;
            foreach ($arr_of_nums as $num) {
                $result_num = $result_num - $num;
            }
            echo "Разность (сумма чисел с обратным знаком): $result_num<br>";
            break;
        case '*':
            $multiple_result = $arr_of_nums[0];
            for ($i = 1; $i < count($arr_of_nums); $i++) {
                $multiple_result = $multiple_result * $arr_of_nums[$i];
            }
            echo "Умножение: $multiple_result<br>";
            break;
        case '/':
            $division_result = $arr_of_nums[0];
            for ($i = 1; $i < count($arr_of_nums); $i++) {
                $division_result = $division_result / $arr_of_nums[$i];
            }
            echo "Деление: $division_result<br>";
            break;
        case '%':
            $mod_division_result = $arr_of_nums[0];
            for ($i = 1; $i < count($arr_of_nums); $i++) {
                $mod_division_result = $mod_division_result % $arr_of_nums[$i];
            }
            echo "Деление по модулю: $mod_division_result <br>";
            break;
        default:
            echo 'Error: Неправильный знак операции';
            return false;
    }
}

arr_calc([1,2,3,4,5], '+');
arr_calc([-1,-2,-3,-4,5], '-');
arr_calc([1,2,3,4,5], '*');
arr_calc([10, 2, 5], '/');
arr_calc([5, 4], '%');