$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('body').on('submit', '#search-form', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/search',
            type: 'GET',
            dataType: 'json',
            data: {
                'search': $('#search-form input').val(),
                'type'  : 'all'},
            success: function (response) {
                if (response['success'] == 'true') {
                    $('#searchResults').show();
                    $('#searchResultTitle').text($('#search-form input').val());
                    if (response['goods'] !== undefined && response['goods'].length > 0) {
                        $('#searchGoodsList').show();
                        $('#searchGoodsList').html(response['goods']);
                    }
                    if (response['news'] !== undefined && response['news'].length > 0) {
                        $('#searchNewsList').show();
                        $('#searchNewsList').html(response['news']);
                    }
                    $('#searchResults').arcticmodal({
                        afterClose: function (data, el) {
                            $('#searchResults').hide();
                            $('#searchGoodsList').html('');
                            $('#searchNewsList').html('');
                        }
                    });
                    $('#search-form input').val('');
                } else {
                    $.ajax({
                        url: '/last_goods',
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            $('.products-columns').html(response['message']);
                        }
                    });
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    });
                }
                // $('.content-main__container').replaceWith(response);
            }
        })
    });

    $('body').on('click', '[data-order]', function(e){
        e.preventDefault();
        $('#buy-form').show();
        $('#buy-form').arcticmodal({
            afterClose: function (data, el) {
                $('#buy-form').hide();
                $('#buy-form')[0].reset();
            }
        });
    });

    $('body').on('click', '[data-buy-one-good-button]', function(e) {
        e.preventDefault();
        $('#buy-form').show();
        if ($('#buy-form [name="one-good-id"]').length == 0) {
            $('#buy-form').append($('<input type="hidden" name="one-good-id" value="' + $(this).attr('data-good-id') + '">'));
        }
        $('#buy-form').arcticmodal({
            afterClose: function (data, el) {
                $('#buy-form').hide();
                $('#buy-form')[0].reset();
            }
        });
    });

    $('body').on('click', '[data-buy-button]', function(e){
        e.preventDefault();

        $.ajax({
            url: '/add_to_basket/' + $(this).attr('data-good-id'),
            dataType: 'json',
            success: function(response){
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        });
    });

    $('body').on('submit', '#buy-form', function(e){
        e.preventDefault();
        var fd = $('#buy-form').serialize();
        $.ajax({
            url: '/orders/create',
            dataType: 'json',
            data: fd,
            success: function(response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    window.location = '/';
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('click', '[data-modal-close]', function (e) {
        e.preventDefault();
        $(this).closest('[data-modal-form]').arcticmodal('close');
    });

    $('body').on('submit', '#delete-form', function (e) {
        e.preventDefault();
        var id = $(this).find('[name=id]').val(),
            type = $(this).attr('data-form-type');
        $.ajax({
            url: '/' + type + '/' + id,
            type: 'DELETE',
            dataType: 'json',
            success: function (response) {
                if (response['success'] == 'true') {
                    $('#delete-form').arcticmodal('close');
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('click', '[data-action-button]', function (e) {
        e.preventDefault();
        var data = [],
            head,
            tblType = $(this).closest('table[data-manager-table]').attr('data-type'),
            pathType,
            id = $(this).closest('tr').attr('data-rec-id'),
            heads = $('table[data-manager-table] th');
        $(this).closest('tr').find('td').each(function (i, el) {
            head = $(heads[i]).attr('data-machine-name');
            if (head !== undefined) {
                data[head] = $(el).text();
            }
        });

        switch ($(this).attr('data-action')) {
            case 'add':
                $('#add-form').show();
                $('#add-form [name="add-rec-type"]').attr('selected', 'selected');
                $('#add-form [data-add-form-item]').hide();
                $('#add-form [data-add-form-item]').removeClass('active');
                switch (location.pathname) {
                    case '/manager/goods':
                        $('#add-form [data-add-form-type="goods"]').show();
                        $('#add-form [data-add-form-type="goods"]').addClass('active');
                        $('form[data-add-form-type="goods"] select[name="category"] option:first-child').attr("selected", "selected");
                        $('#add-form-type option[value="good"]').attr("selected", "selected");
                        pathType = 'goods';
                        break;
                    case '/manager/news':
                        $('#add-form [data-add-form-type="news"]').show();
                        $('#add-form [data-add-form-type="news"]').addClass('active');
                        $('#add-form-type option[value="news"]').attr("selected", "selected");
                        pathType = 'news';
                        break;
                    case '/manager/categories':
                    default:
                        $('#add-form [data-add-form-type="categories"]').show();
                        $('#add-form [data-add-form-type="categories"]').addClass('active');
                        $('#add-form-type option[value="category"]').attr("selected", "selected");
                        pathType = 'categories';
                }
                $('#add-form').arcticmodal({
                    beforeClose: function(data, el) {
                        $.ajax({
                            url: '/manager/' + pathType + '/table',
                            type: 'GET',
                            success: function (response) {
                                $('table[data-manager-table] tbody').html(response);
                            }
                        })
                    },
                    afterClose: function (data, el) {
                        $('#add-form').hide();
                    }
                });
                break;
            case 'edit':
                switch (tblType) {
                    case 'category':
                        $('#edit-category-form [name="id"]').val(id);
                        $('#edit-category-form [name="name"]').val(data['name']);
                        $('#edit-category-form [name="category"]').val(data['category']);
                        $('#edit-category-form [name="description"]').val(data['description']);
                        $('#edit-category-form [name="price"]').val(data['price']);
                        $('#edit-category-form [name="photo"]').val(data['photo']);
                        $('#edit-category-form').show();
                        $('#edit-category-form').arcticmodal({
                            beforeClose: function(data, el) {
                                $.ajax({
                                    url: '/manager/categories/table',
                                    type: 'GET',
                                    success: function (response) {
                                        $('table[data-manager-table] tbody').html(response);
                                    }
                                })
                            },
                            afterClose: function (data, el) {
                                $('#edit-category-form').hide();
                                $('#edit-category-form')[0].reset();
                            }
                        });
                        break;
                    case 'good':
                        $('#edit-good-form [name="id"]').val(id);
                        $('#edit-good-form [name="name"]').val(data['name']);
                        $('#edit-good-form [name="description"]').val(data['description']);
                        $('#edit-good-form [name="price"]').val(data['price']);
                        $('#edit-good-form').show();
                        $('#edit-good-form').arcticmodal({
                            beforeClose: function(data, el) {
                                $.ajax({
                                    url: '/manager/goods/table',
                                    type: 'GET',
                                    success: function (response) {
                                        $('table[data-manager-table] tbody').html(response);
                                    }
                                })
                            },
                            afterClose: function (data, el) {
                                $('#edit-good-form').hide();
                                $('#edit-good-form')[0].reset();
                            }
                        });
                        break;
                    case 'news':
                        $('#edit-news-form [name="id"]').val(id);
                        $('#edit-news-form [name="title"]').val(data['title']);
                        $('#edit-news-form [name="body"]').val(data['body']);
                        $('#edit-news-form').show();
                        $('#edit-news-form').arcticmodal({
                            beforeClose: function(data, el) {
                                $.ajax({
                                    url: '/manager/news/table',
                                    type: 'GET',
                                    success: function (response) {
                                        $('table[data-manager-table] tbody').html(response);
                                    }
                                })
                            },
                            afterClose: function (data, el) {
                                $('#edit-news-form').hide();
                                $('#edit-news-form')[0].reset();
                            }
                        });
                        break;
                }
                break;
            case 'delete':
                $('#delete-form [name="id"]').val(id);
                switch (tblType) {
                    case 'category':
                        $('#delete-form [data-title]').text('Вы действительно хотите удалить эту категорию?');
                        $('#delete-form').attr('data-form-type', 'categories');
                        pathType = 'categories';
                        break;
                    case 'good':
                        $('#delete-form [data-title]').text('Вы действительно хотите удалить этот товар?');
                        $('#delete-form').attr('data-form-type', 'goods');
                        pathType = 'goods';
                        break;
                    case 'news':
                        $('#delete-form [data-title]').text('Вы действительно хотите удалить эту новость?');
                        $('#delete-form').attr('data-form-type', 'news');
                        pathType = 'news';
                        break;
                }
                $('#delete-form').show();
                $('#delete-form').arcticmodal({
                    beforeClose: function(data, el) {
                        $.ajax({
                            url: '/manager/' + pathType + '/table',
                            type: 'GET',
                            success: function (response) {
                                $('table[data-manager-table] tbody').html(response);
                            }
                        })
                    },
                    afterClose: function (data, el) {
                        $('#delete-form').hide();
                        $('#delete-form')[0].reset();
                        $('#delete-form').attr('data-form-type', '');
                    }
                });
                break;
        }
    });

    $('body').on('submit', '#email_settings', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/manager/set_email_settings',
            type: 'GET',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    });
                }
            },
            error: function(response) {
                var message = '';
                responseArr = JSON.parse(response['responseText']);
                for(var index in responseArr) {
                    message += responseArr[index] + '<br>';
                }

                new PNotify({
                    title: 'Ошибка',
                    type: 'error',
                    text: message
                })
            }
        })
    });
    $('body').on('submit', '#edit-news-form', function (e) {
        e.preventDefault();
        var fd = new FormData;
        fd.append('title', $(this).find('[name=title]').val());
        fd.append('body', $(this).find('[name=body]').val());
        var file = $(this).find('input[name="photo"]')[0].files[0];
        fd.append('photo', file);
        $.ajax({
            url: '/news/' + $('#edit-news-form input[name="id"]').val(),
            type: 'POST',
            data: fd,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (response) {
                if (response['success'] == 'true') {
                    $('#edit-news-form').arcticmodal('close');
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            },
            error: function(response) {
                var message = '';
                responseArr = JSON.parse(response['responseText']);
                for(var index in responseArr) {
                    message += responseArr[index] + '<br>';
                }

                new PNotify({
                    title: 'Ошибка',
                    type: 'error',
                    text: message
                })
            }
        })
    });

    $('body').on('submit', '#edit-category-form', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            url: '/categories/' + $('#edit-category-form input[name="id"]').val(),
            type: 'PUT',
            data: data,
            dataType: 'json',
            success: function (response) {
                if (response['success'] == 'true') {
                    $('#edit-category-form').arcticmodal('close');
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            },
            error: function(response) {
                var message = '';
                responseArr = JSON.parse(response['responseText']);
                for(var index in responseArr) {
                    message += responseArr[index] + '<br>';
                }

                new PNotify({
                    title: 'Ошибка',
                    type: 'error',
                    text: message
                })
            }
        })
    });

    $('body').on('submit', '#edit-good-form', function (e) {
        e.preventDefault();
        var fd = new FormData;
        fd.append('name', $(this).find('[name=name]').val());
        fd.append('category', $(this).find('[name=category]').val());
        fd.append('description', $(this).find('[name=description]').val());
        fd.append('price', $(this).find('[name=price]').val());
        var file = $(this).find('input[name="photo"]')[0].files[0];
        fd.append('photo', file);
        $.ajax({
            url: '/goods/' + $('#edit-good-form input[name="id"]').val(),
            type: 'POST',
            data: fd,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (response) {
                if (response['success'] == 'true') {
                    $('#edit-good-form').arcticmodal('close');
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            },
            error: function(response) {
                var message = '';
                responseArr = JSON.parse(response['responseText']);
                for(var index in responseArr) {
                    message += responseArr[index] + '<br>';
                }

                new PNotify({
                    title: 'Ошибка',
                    type: 'error',
                    text: message
                })
            }
        })
    });

    $('body').on('change', '#add-form-type', function (e) {
         switch ($(this).val()) {
             case 'category':
                 $('[data-add-form-item]').removeClass('active').hide();
                 $('[data-add-form-type=categories]').addClass('active').show();
                 break;
             case 'news':
                 $('[data-add-form-item]').removeClass('active').hide();
                 $('[data-add-form-type=news]').addClass('active').show();
                 break;
             case 'good':
                 $('[data-add-form-item]').removeClass('active').hide();
                 $('[data-add-form-type=goods]').addClass('active').show();
                 break;
         }
    });

    $('body').on('submit', 'form[data-add-form-item]', function (e) {
        e.preventDefault();
        var fd = '',
            file = '',
            method = '',
            callback,
            type = $(this).attr('data-add-form-type');
        switch (type) {
            case 'categories':
                    fd = $(this).serialize();
                    method = 'GET';
                break;
            case 'goods':
                    fd = new FormData;
                    file = $(this).find('input[name="photo"]')[0].files[0];
                    method = 'POST';
                fd.append('name', $(this).find('input[name="name"]').val());
                fd.append('cat_id', $(this).find('select[name="category"]').val());
                fd.append('description', $(this).find('textarea[name="description"]').val());
                fd.append('price', $(this).find('input[name="price"]').val());
                fd.append('photo', file);
                callback = function(response) {
                    var err_key;
                    if (array_key_exists('name', response)) {
                        err_key = 'name';
                    }
                    if (array_key_exists('cat_id', response)) {
                        err_key = 'cat_id';
                    }
                    if (array_key_exists('price', response)) {
                        err_key = 'price';
                    }
                    if (array_key_exists('photo', response)) {
                        err_key = 'photo';
                    }
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response[err_key]
                    })
                };
                break;
            case 'news':
                    fd = new FormData;
                    file = $(this).find('input[name="photo"]')[0].files[0];
                    method = 'POST';
                    fd.append('title', $(this).find('input[name="title"]').val());
                    fd.append('body', $(this).find('textarea[name="body"]').val());
                    fd.append('photo', file);
                break;
        }

        $.ajax({
            url: '/' + type + '/create',
            type: method,
            data: fd,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (response) {
                $('form[data-add-form-type="' + type + '"]')[0].reset();
                new PNotify({
                    title: 'Успех',
                    type: 'success',
                    text: response['message']
                });
            },
            error: function(response) {
                var message = '';
                responseArr = JSON.parse(response['responseText']);
                for(var index in responseArr) {
                    message += responseArr[index] + '<br>';
                }

                new PNotify({
                    title: 'Ошибка',
                    type: 'error',
                    text: message
                })
            }
        })
    });
});