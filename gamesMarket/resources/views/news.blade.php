@extends('layouts.app')

@section('content')
    @include('parts.content-top')
    <div class="content-middle">
        <div class="content-head__container">
            <div class="content-head__title-wrap">
                <div class="content-head__title-wrap__title bcg-title">Последние новости</div>
            </div>
            @include('parts.search')
        </div>
        <div class="content-main__container">
            <div class="news-list__container">
                @foreach ($news_list as $news)
                    <div class="news-list__item">
                        <div class="news-list__item__thumbnail"><img src="{{ $news->photo }}"></div>
                        <div class="news-list__item__content">
                            <div class="news-list__item__content__news-title">{{ $news->title }}</div>
                            <div class="news-list__item__content__news-date">{{ $news->created_at }}</div>
                            <div class="news-list__item__content__news-content">
                                {{ $news->body }}
                            </div>
                        </div>
                        <div class="news-list__item__content__news-btn-wrap"><a href="news/{{ $news->id }}" class="btn btn-brown">Подробнее</a></div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="content-footer__container">
        {{ $news_list->links() }}
        </div>
    </div>
    <div class="content-bottom"></div>
@endsection
