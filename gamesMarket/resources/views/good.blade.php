@extends('layouts.app')

@section('scripts')
    @include('parts.bootstrapjs')
@endsection
@section('styles')
    @include('parts.bootstrapcss')
@endsection

@section('content')
    @include('parts.content-top')
    <div class="content-middle">
        <div class="content-head__container">
            <div class="content-head__title-wrap">
                <div class="content-head__title-wrap__title bcg-title">
                    {{$good->name}} в разделе {{$good->category->name}}
                </div>
            </div>
            @include('parts.search')
        </div>
        <div class="product-container">
            <div class="product-container__image-wrap">
                <img src="{{ $good->photo }}" class="image-wrap__image-product">
            </div>
            <div class="product-container__content-text">
                <div class="product-container__content-text__title">{{ $good->name }}</div>
                <div class="product-container__content-text__price">
                    <div class="product-container__content-text__price__value">
                        Цена: <b>{{ $good->price }}</b>
                        руб
                    </div>
                    <a href="#" data-buy-one-good-button class="btn btn-blue" data-good-id="{{ $good->id }}">Купить</a>
                </div>
                <div class="product-container__content-text__description">
                    <p>{{ $good->description }}</p>
                </div>
            </div>
        </div>
    </div>
    @include('parts.content-bottom', ['good' => $good, 'see_also' => $see_also])
@endsection