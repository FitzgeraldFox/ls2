@extends('layouts.app')

@section('scripts')
    @include('parts.bootstrapjs')
@endsection
@section('styles')
    @include('parts.bootstrapcss')
@endsection

@section('content')
    @include('parts.content-top')
    <div class="content-middle">
        <div class="content-head__container">
            <div class="content-head__title-wrap">
                <div class="content-head__title-wrap__title bcg-title">Мои заказы</div>
            </div>
            @include('parts.search')
        </div>
        <div class="content-main__container">
            <div class="cart-product-list">
                @if (!empty($goods) && count($goods) > 0)
                    @foreach($goods as $good)
                        @if ($good !== null)
                            <div class="cart-product-list__item" data-item-id="{{ $good->id }}">
                            <div class="cart-product__item__product-photo">
                                <img src="{{ $good->photo }}" class="cart-product__item__product-photo__image">
                            </div>
                            <div class="cart-product__item__product-name">
                                <div class="cart-product__item__product-name__content">
                                    <a href="/goods/{{ $good->id }}">{{ $good->name }}</a>
                                </div>
                            </div>
                            <div class="cart-product__item__cart-date">
                                <div class="cart-product__item__cart-date__content">{{ date_format(date_create($good->created_at), 'd.m.Y') }}</div>
                            </div>
                            <div class="cart-product__item__product-price">
                                <span class="product-price__value">{{ $good->price }} рублей</span>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    @if (isset($goods_sum) && !empty($goods_sum))
                        <div class="cart-product-list__result-item">
                            <div class="cart-product-list__result-item__text">Итого</div>
                            <div class="cart-product-list__result-item__value">{{ $goods_sum }} рублей</div>
                        </div>
                    @endif

                @else
                    <p>Ваша корзина пуста</p>
                @endif
            </div>
            @if (!empty($goods))
                <div class="content-footer__container">
                    <div class="btn-buy-wrap"><a href="#" data-order class="btn-buy-wrap__btn-link">Перейти к оплате</a></div>
                </div>
            @endif
        </div>
        @if (!empty($goods))
            <div class="content-footer__container">
            {{ $goods->links() }}
            </div>
        @endif
    </div>
@endsection