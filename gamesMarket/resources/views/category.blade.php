@extends('layouts.app')

@section('content')
    @include('parts.content-top')
    <div class="content-middle">
        <div class="content-head__container">
            <div class="content-head__title-wrap">
                <div class="content-head__title-wrap__title bcg-title">Игры в разделе {{ $category->name }}</div>
            </div>
            @include('parts.search')
        </div>
        <div class="content-main__container">
            <div class="products-category__list">
                @foreach($goods as $good)
                    <div class="products-category__list__item">
                        <div class="products-category__list__item__title-product">
                            <a href="/goods/{{ $good->id }}">{{ $good->name }}</a>
                        </div>
                        <div class="products-category__list__item__thumbnail">
                            <a href="/goods/{{ $good->id }}" class="products-category__list__item__thumbnail__link">
                                <img src="{{ $good->photo }}" alt="Preview-image">
                            </a>
                        </div>
                        <div class="products-category__list__item__description">
                            <span class="products-price">{{ $good->price }} руб.</span>
                            @include('parts.order_button', ['good_id' => $good->id])
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="content-footer__container">
            {{ $goods->links() }}
        </div>
    </div>
    <div class="content-bottom"></div>
@endsection
