@extends('layouts.app')

@section('content')
    @include('parts.content-top')
    <div class="content-middle">
        <div class="content-head__container">
            <div class="content-head__title-wrap">
                <div class="content-head__title-wrap__title bcg-title">Последние товары</div>
            </div>
            @include('parts.search')
        </div>
        @include('parts.lastGoods')
    </div>
    <div class="content-bottom"></div>
@endsection
