@extends('layouts.app')

@section('content')
    @include('parts.content-top')
    <div class="content-middle">
        <div class="content-head__container">
            <div class="content-head__title-wrap">
                <div class="content-head__title-wrap__title bcg-title">Новости</div>
            </div>
            @include('parts.search')
        </div>
        <div class="content-main__container">
            <div class="news-block content-text">
                <h3 class="content-text__title">
                    {{ $news->title }}
                </h3><img src="{{ $news->photo }}" alt="Image" class="alignleft img-news">
                {{ $news->body }}
            </div>
        </div>
    </div>
    <div class="content-bottom"></div>
@endsection
