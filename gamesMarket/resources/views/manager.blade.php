@extends('layouts.app')

@section('scripts')
    @include('parts.bootstrapjs')
@endsection
@section('styles')
    @include('parts.bootstrapcss')
@endsection

@section('content')
    <div class="content-middle">
        <div class="content-main__container">
            <h1>Панель управления</h1>
            @include('parts.manager-menu')
            <h2>Заказы</h2>
            <table data-manager-table data-type="news" class="table table-striped">
                <thead>
                <th>Email заказчика</th>
                <th>Имя заказчика</th>
                <th>Содержимое заказа</th>
                <th>Сумма заказа, руб.</th>
                <th>Дата оформления заказа</th>
                </thead>
                <tbody>
                @include('manager.ordersTable', ['orders' => $orders, 'elems_orders' => $elems_orders, 'sum' => $sum])
                </tbody>
            </table>
        </div>
    </div>
    <div class="content-bottom"></div>
@endsection
