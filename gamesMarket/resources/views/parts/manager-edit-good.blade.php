<form id="edit-good-form" class="data-modal-manager-form" data-modal-form>
    {{ csrf_field() }}
    <input type="hidden" name="id">
    <div class="form-group">
        <label>
            <p>Имя</p>
            <input type="text" name="name" class="form-control">
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Категория</p>
            <select name="category" class="form-control">
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Описание</p>
            <textarea name="description" class="form-control"></textarea>
        </label>
    </div>

    <div class="form-group">
        <label>
            <p>Цена</p>
            <input type="number" min="0" name="price" class="form-control">
        </label>
    </div>

    <div class="form-group">
        <label>
            <p>Фото</p>
            <input type="file" name="photo">
        </label>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">Редактировать</button>
    </div>
    @include('parts.modal-close')

</form>