<div class="news-container">
    @if (isset($news) && !empty($news))
    <h1>Новости</h1>

    <div class="products-columns__items">
        @include('parts.news_list', ['news' => $news])
    </div>
    @endif
</div>