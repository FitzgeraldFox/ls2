<div id="searchResults" data-modal-form>
    <h1>Результаты поиска по фразе <span id="searchResultTitle"></span></h1>
    <div id="searchResultList">
        <div id="searchGoodsList"></div>
        <div id="searchNewsList"></div>
    </div>
    @include('parts.modal-close')
</div>