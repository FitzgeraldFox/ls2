<div class="goods-container">
    <div class="products-columns__items">
        @include('parts.goods_list', ['goods' => $goods])
    </div>
    <div class="content-footer__container">
        {{ $goods->links() }}
    </div>
</div>