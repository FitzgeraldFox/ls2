<div class="goods-container">
    <div class="products-columns__items">
        @foreach($categories as $category)
            <div class="products-columns__item">
                <div class="products-columns__item__title-product">
                    <a href="/categories/{{ $category->id }}" class="products-columns__item__title-product__link">{{ $category->name }}</a>
                </div>
                <div class="products-columns__item__description">
                    {{ $category->description }}
                </div>
            </div>
        @endforeach
    </div>
    <div class="content-footer__container">
        {{ $categories->links() }}
    </div>
</div>