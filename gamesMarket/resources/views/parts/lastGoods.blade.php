<div class="content-main__container">
    <div class="products-columns">
        @if (isset($goods) && count($goods) > 0)
            @include('parts.goods_container')
        @endif
        @if (isset($news) && count($news) > 0)
            @include('parts.news_container')
        @endif
    </div>
</div>

