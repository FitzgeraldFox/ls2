<form data-add-form-item data-add-form-type="goods">
    <div class="form-group">
        <label>
            <p>Имя</p>
            <input type="text" name="name" required class="form-control">
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Категория</p>
            <select name="category" required class="form-control">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Описание</p>
            <textarea name="description" class="form-control"></textarea>
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Цена</p>
            <input class="form-control" type="number" min="0" name="price" required>
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Фото</p>
            <input type="file" name="photo">
        </label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">Создать товар</button>
    </div>
    @include('parts.modal-close')
</form>