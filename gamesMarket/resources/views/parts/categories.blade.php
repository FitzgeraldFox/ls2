<div class="content-main__container">
    <div class="products-columns">
        @if (isset($categories) && count($categories) > 0)
            @include('parts.categories_container')
        @else
            <p>Список категорий пуст</p>
        @endif
    </div>
</div>

