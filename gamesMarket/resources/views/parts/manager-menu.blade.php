<ul class="list-group manager-menu">
    <li class="list-group-item"><a href="{{ route('manager-categories') }}">Категории</a></li>
    <li class="list-group-item"><a href="{{ route('manager-goods') }}">Товары</a></li>
    <li class="list-group-item"><a href="{{ route('manager-orders') }}">Заказы</a></li>
    <li class="list-group-item"><a href="{{ route('manager-news') }}">Новости</a></li>
    <li class="list-group-item"><a href="{{ route('manager-email') }}">Настройки почты</a></li>
</ul>