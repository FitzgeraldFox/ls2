@if(isset($news) > 0)

<div class="news-container">
    <div class="products-columns__items">
        @include('parts.news_list', ['news' => $news])
    </div>
    <div class="content-footer__container">
        {{ $news->links() }}
    </div>
</div>
@endif