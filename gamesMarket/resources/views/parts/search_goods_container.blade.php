<div class="goods-container">
    @if (isset($goods) && !empty($goods))
    <h1>Товары</h1>
    <div class="products-columns__items">
        @include('parts.goods_list', ['goods' => $goods])
    </div>
    @endif
</div>