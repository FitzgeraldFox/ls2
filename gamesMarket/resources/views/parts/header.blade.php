<header class="main-header">
    <div class="logotype-container"><a href="{{ url('/') }}" class="logotype-link"><img src="{{ asset('img/logo.png') }}" alt="Логотип"></a></div>
    <nav class="main-navigation">
        <ul class="nav-list">
            <li class="nav-list__item"><a href="{{ url('/') }}" class="nav-list__item__link">Главная</a></li>
            <li class="nav-list__item"><a href="{{ url('/news') }}" class="nav-list__item__link">Новости</a></li>
            <li class="nav-list__item"><a href="{{ url('/about') }}" class="nav-list__item__link">О компании</a></li>
        </ul>
    </nav>
    <div class="header-contact">
        <div class="header-contact__phone">
            <a href="#" class="header-contact__phone-link">Телефон: 33-333-33</a>
        </div>
    </div>
    <div class="header-container">
        <div class="payment-container">
            <div class="payment-basket__status">
                <div class="payment-basket__status__icon-block">
                    <a href='/basket' class="payment-basket__status__icon-block__link">
                        <i class="fa fa-shopping-basket"></i>
                    </a>
                </div>
                <div class="payment-basket__status__basket">
                    <span class="payment-basket__status__basket-value">{{ $goods_count | 0 }}</span>
                    <span class="payment-basket__status__basket-value-descr">товаров</span>
                </div>
            </div>
        </div>
        <div class="authorization-block">
            @if (Auth::guest())
                <a href="{{ route('register') }}" class="authorization-block__link">Регистрация</a>
                <a href="{{ route('login') }}" class="authorization-block__link">Войти</a>
            @else
                <a href="{{ route('logout') }}"
                   class="authorization-block__link"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    Выйти
                </a>
                @if ($is_admin == 1)
                    <a href="{{ route('manager') }}" class="authorization-block__link">Панель управления</a>
                @endif
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif
        </div>
    </div>
</header>