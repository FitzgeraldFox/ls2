<form data-add-form-item data-add-form-type=categories>
    <div class="form-group">
        <label>
            <p>Имя</p>
            <input type="text" class="form-control" name="name" required>
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Описание</p>
            <textarea type="text" class="form-control" name="description"></textarea>
        </label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">Создать категорию</button>
    </div>
    @include('parts.modal-close')
</form>