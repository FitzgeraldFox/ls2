<form id="delete-form" class="data-modal-manager-form" data-modal-form>
    {{ csrf_field() }}
    <div class="form-group">
        <h1 data-title></h1>
    </div>
    <div class="form-group">
        <button type="submit"  class="btn btn-default">Да</button>
    </div>
    <input type="hidden" name="id">
    @include('parts.modal-close')
</form>