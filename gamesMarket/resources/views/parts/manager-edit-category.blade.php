<form id="edit-category-form" class="data-modal-manager-form" data-modal-form>
    {{ csrf_field() }}
    <input type="hidden" name="id">
    <div class="form-group">
        <label>
            <p>Имя</p>
            <input type="text" name="name" class="form-control">
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Описание</p>
            <textarea type="text" name="description" class="form-control"></textarea>
        </label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">Редактировать</button>
    </div>
    @include('parts.modal-close')
</form>