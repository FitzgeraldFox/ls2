<form data-add-form-item data-add-form-type="news">
    <div class="form-group">
        <label>
            <p>Заголовок *</p>
            <input type="text" name="title" required class="form-control">
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Текст новости *</p>
            <textarea name="body" class="form-control" required></textarea>
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Фото</p>
            <input type="file" name="photo">
        </label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">Создать новость</button>
    </div>
    @include('parts.modal-close')
</form>