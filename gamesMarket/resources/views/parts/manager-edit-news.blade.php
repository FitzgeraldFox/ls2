<form id="edit-news-form" class="data-modal-manager-form" data-modal-form>
    {{ csrf_field() }}
    <input type="hidden" name="id">
    <div class="form-group">
        <label>
            <p>Заголовок</p>
            <input type="text" name="title" class="form-control">
        </label>
    </div>

    <div class="form-group">
        <label>
            <p>Текст новости</p>
            <textarea type="text" name="body" class="form-control"></textarea>
        </label>
    </div>

    <div class="form-group">
        <label>
            <p>Фото новости</p>
            <input type="file" name="photo">
        </label>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-success">Редактировать</button>
    </div>
    @include('parts.modal-close')
</form>