<div class="sidebar">
    <div class="sidebar-item">
        <div class="sidebar-item__title">
            <a href="/categories" class="linkToPage">
                Категории
            </a>
        </div>
        <div class="sidebar-item__content">
            <ul class="sidebar-category">
                @foreach ($categories as $category)
                    <li class="sidebar-category__item"><a href="/categories/{{ $category['id'] }}" class="sidebar-category__item__link">{{ $category['name'] }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="sidebar-item">
        <div class="sidebar-item__title">
            <a href="/news" class="linkToPage">
                Последние новости
            </a>
        </div>
        <div class="sidebar-item__content">
            <div class="sidebar-news">
                @foreach ($last_news as $news)
                    <div class="sidebar-news__item">
                        <div class="sidebar-news__item__preview-news">
                            <img src="{{ $news['photo'] }}" alt="image-new" class="sidebar-new__item__preview-new__image">
                        </div>
                        <div class="sidebar-news__item__title-news">
                            <a href="/news/{{ $news['id'] }}" class="sidebar-news__item__title-news__link">{{ $news['title'] }}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>