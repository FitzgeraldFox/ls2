<div id="add-form" class="data-modal-manager-form" data-modal-form>
    {{ csrf_field() }}
    <div class="form-group">
        <select id="add-form-type" name="add-rec-type" class="form-control">
            <option value="category">Категория</option>
            <option value="good">Товар</option>
            <option value="news">Новости</option>
        </select>
    </div>
    <div class="form-group">
        <div id="add-form-container">
            @include('parts.add-cat-form')
            @include('parts.add-good-form')
            @include('parts.add-news-form')
        </div>
    </div>
    @include('parts.modal-close')
</div>