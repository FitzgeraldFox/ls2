<form id="buy-form" data-modal-form>
    <h1>Заполните контактные данные</h1>

    <div class="form-group">
        <label>
            <p>Имя:</p>
            <input class='form-control' type="text" name="name">
        </label>
    </div>
    <div class="form-group">
        <label>
            <p>Email:</p>
            <input type="email" class='form-control' value="@if( Auth::check() ){{ Auth::user()->email }}@endif" name="email">
        </label>
    </div>
    <div class="form-group">
        <button type="submit" id="buy-form-submit" class="btn btn-success">Оформить заказ</button>
    </div>
    @include('parts.modal-close')
</form>