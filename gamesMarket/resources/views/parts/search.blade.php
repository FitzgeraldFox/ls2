<div class="content-head__search-block">
    <div class="search-container">
        <form class="search-container__form" id="search-form" data-type="
        @isset($searchType)
            {{ $searchType }}
        @endisset
        @empty($searchType)
            goods
        @endempty
            ">
            <input type="text" class="search-container__form__input">
            <button class="search-container__form__btn">search</button>
        </form>
    </div>
</div>