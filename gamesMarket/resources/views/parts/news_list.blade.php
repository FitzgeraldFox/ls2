@foreach($news as $new)
    <div class="products-columns__item">
        <div class="products-columns__item__title-product">
            <a href="/news/{{ $new->id }}" class="products-columns__item__title-product__link">{{ $new->title }}</a>
        </div>
        <div class="products-columns__item__thumbnail">
            <a href="/news/{{ $new->id }}" class="products-columns__item__thumbnail__link">
                <img src="{{ $new->photo }}" alt="Preview-image" class="products-columns__item__thumbnail__img">
            </a>
        </div>
    </div>
@endforeach