@foreach($goods as $good)
<div class="products-columns__item">
    <div class="products-columns__item__title-product">
        <a href="/goods/{{ $good->id }}" class="products-columns__item__title-product__link">{{ $good->name }}</a>
    </div>
    <div class="products-columns__item__thumbnail">
        <a href="/goods/{{ $good->id }}" class="products-columns__item__thumbnail__link">
            <img src="{{ $good->photo }}" alt="Preview-image" class="products-columns__item__thumbnail__img">
        </a>
    </div>
    <div class="products-columns__item__description">
        <span class="products-price">{{ $good->price }} руб</span>
        @include('parts.order_button', ['good_id' => $good->id])
    </div>
</div>
@endforeach