<div class="content-bottom">
    <div class="line"></div>
    <div class="content-head__container">
        <div class="content-head__title-wrap">
            <div class="content-head__title-wrap__title bcg-title">Посмотрите наши товары</div>
        </div>
    </div>
    <div class="content-main__container">
        <div class="products-columns">
            @foreach($see_also as $also_good)
                <div class="products-columns__item">
                    <div class="products-columns__item__title-product"><a href="/goods/{{ $also_good->id }}" class="products-columns__item__title-product__link">{{ $also_good->name }}</a></div>
                    <div class="products-columns__item__thumbnail"><a href="/goods/{{ $also_good->id }}" class="products-columns__item__thumbnail__link"><img src="{{ $also_good->photo }}" alt="Preview-image" class="products-columns__item__thumbnail__img"></a></div>
                    <div class="products-columns__item__description"><span class="products-price">{{ $also_good->price }} руб</span>
                        @include('parts.order_button', ['good_id' => $also_good->id])
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>