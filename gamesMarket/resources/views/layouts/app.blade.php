<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
@include('parts.head')
<body>
    <div class="main-wrapper">
        @include('parts.header')
        <div class="middle">
            @include('parts.sidebar')
            <div class="main-content">
                @yield('content')
            </div>
        </div>
        @include('parts.footer')
    </div>
    @include('parts.search-results')
    @include('parts.buy-form')
    <!-- Scripts -->
    <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.js"></script>
    <script src="{{ asset('js/vendor/jquery.arcticmodal-0.3/jquery.arcticmodal-0.3.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    @yield('scripts')
</body>
</html>
