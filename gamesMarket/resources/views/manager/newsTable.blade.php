@forelse($news_list as $news)
    <tr data-rec-id="{{ $news['id'] }}">
        <td>{{ $news['title'] }}</td>
        <td>{{ str_limit($news['body'], 100, '...') }}</td>
        <td>
            <img src="{{ $news['photo'] }}">
        </td>
        <td>
            <button data-action-button data-action="edit" class="btn btn-default">Редактировать</button>
            <button data-action-button data-action="delete" class="btn btn-default">Удалить</button>
        </td>
    </tr>
@empty
    <tr><td colspan="4">Таблица новостей пуста</td></tr>
@endforelse