@extends('layouts.app')

@section('scripts')
    @include('parts.bootstrapjs')
@endsection
@section('styles')
    @include('parts.bootstrapcss')
@endsection

@section('content')
    <div class="content-middle">
        <div class="content-main__container">
            <h1>Товары</h1>
            @include('parts.manager-menu')
            @include('parts.manager-actions-panel')
            <table data-manager-table data-type="good" class="table table-striped">
                <thead>
                <th data-machine-name="name">Имя</th>
                <th data-machine-name="category">Категория</th>
                <th data-machine-name="description">Описание</th>
                <th data-machine-name="price">Цена</th>
                <th data-machine-name="photo">Фото</th>
                <th>Операции</th>
                </thead>
                <tbody>
                    @include('manager.goodsTable', ['goods' => $goods])
                </tbody>
            </table>
            @include('parts.manager-add-form')
            @include('parts.manager-edit-good')
            @include('parts.manager-delete-form')
        </div>
    </div>
    <div class="content-bottom"></div>
@endsection
