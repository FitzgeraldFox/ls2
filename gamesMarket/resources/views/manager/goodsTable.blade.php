@forelse($goods as $key => $good)
    <tr data-rec-id="{{ $good->id }}">
        <td>{{ $good->name }}</td>
        <td>{{ $good->category->name }}</td>
        <td>{{ str_limit($good->description, 100, '...') }}</td>
        <td>{{ $good->price }}</td>
        <td><img src="{{ $good->photo }}"></td>
        <td>
            <button data-action-button data-action="edit" class="btn btn-default">Редактировать</button>
            <button data-action-button data-action="delete" class="btn btn-default">Удалить</button>
        </td>
    </tr>
@empty
    <tr><td colspan="6">Таблица товаров пуста</td></tr>
@endforelse