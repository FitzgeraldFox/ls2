@forelse($categories as $category)
    <tr data-rec-id="{{ $category['id'] }}">
        <td>{{ $category['name'] }}</td>
        <td>{{ str_limit($category['description'], 100, '...') }}</td>
        <td>
            <button data-action-button data-action="edit" class="btn btn-default">Редактировать</button>
            <button data-action-button data-action="delete" class="btn btn-default">Удалить</button>
        </td>
    </tr>
@empty
    <tr><td colspan="3">Таблица категорий пуста</td></tr>
@endforelse