@extends('layouts.app')

@section('scripts')
    @include('parts.bootstrapjs')
@endsection
@section('styles')
    @include('parts.bootstrapcss')
@endsection

@section('content')
    <div class="content-middle">
        <div class="content-main__container">
            <h1>Новости</h1>
            @include('parts.manager-menu')
            @include('parts.manager-actions-panel')
            <table data-manager-table data-type="news" class="table table-striped">
                <thead>
                <th data-machine-name="title">Заголовок</th>
                <th data-machine-name="body">Текст новости</th>
                <th data-machine-name="photo">Фото новости</th>
                <th>Операции</th>
                </thead>
                <tbody>
                @include('manager.newsTable', ['news_list' => $news])
                </tbody>
            </table>
            @include('parts.manager-add-form')
            @include('parts.manager-edit-news')
            @include('parts.manager-delete-form')
        </div>
    </div>
    <div class="content-bottom"></div>
@endsection
