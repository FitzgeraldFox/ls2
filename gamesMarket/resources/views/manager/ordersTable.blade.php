@forelse($orders as $order_key => $order)
    <tr>
        <td>{{ $order['email'] }}</td>
        <td>{{ $order['name'] }}</td>
        <td>
            <ul>
                @foreach ($elems_orders[$order_key] as $elem)
                    @if(!empty($elem))
                        <li>{{ $elem['name'] }}</li>
                    @endif
                @endforeach
            </ul>
        </td>
        <td>
            {{ $sum[$order_key] }}
        </td>
        <td>{{ $order->created_at }}</td>
    </tr>
@empty
    <tr><td colspan="4">Таблица заказов пуста</td></tr>
@endforelse