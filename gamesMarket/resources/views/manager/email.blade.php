@extends('layouts.app')

@section('scripts')
    @include('parts.bootstrapjs')
@endsection
@section('styles')
    @include('parts.bootstrapcss')
@endsection

@section('content')
    <div class="content-middle">
        <div class="content-main__container">
            <h1>Настройки Email</h1>
            @include('parts.manager-menu')
            <form id="email_settings">
                <div class="form-group">
                    <label>
                        <p>Список Email администратора</p>
                        <textarea name="email_list">
                            {{ $settings->email_list | '' }}
                        </textarea>
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p>Отсылать администратору на почту уведомление о новых заказах?</p>
                        <input type="checkbox" name="send_admin_email"
                        @if ($settings->send_admin_email == 1)
                        checked
                        @endif
                        >
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
    <div class="content-bottom"></div>
@endsection
