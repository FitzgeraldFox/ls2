<html>
<head></head>
<body>
<h1>Оформлен новый заказ на сайте Games Market</h1>
<h2>Данные заказчика</h2>
<p>Email: {{ $order->email }}</p>
<p>Имя: {{ $order->name }}</p>
<p>Содержимое Заказа:</p>

<ul>
@foreach($order_data as $elem)
    @foreach($elem as $el)
            <li>{{ $el->name }} - {{ $el->price }} руб.</li>
    @endforeach
@endforeach
</ul>
<p>Сумма заказа: {{ $sum }} руб.</p>
<p>Дата оформления заказа: {{ date('d.m.Y H:i') }}</p>
</body>
</html>