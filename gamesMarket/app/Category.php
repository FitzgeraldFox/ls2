<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'description'];

    public function goods()
    {
        return $this->hasMany('App\Good', 'cat_id');
    }
}
