<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailSettings extends Model
{

    protected $fillable = ['id', 'email_list', 'send_admin_email'];

}
