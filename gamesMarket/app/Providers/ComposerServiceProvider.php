<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        view()->composer('parts.sidebar', 'App\Http\ViewComposers\SidebarComposer');
        view()->composer('parts.footer', 'App\Http\ViewComposers\RandomGoodComposer');
        view()->composer('parts.header', 'App\Http\ViewComposers\HeaderComposer');
        view()->composer('basket', 'App\Http\ViewComposers\BasketComposer');
        view()->composer('parts.manager-edit-good', 'App\Http\ViewComposers\ManagerEditGoodComposer');
        view()->composer('parts.add-good-form', 'App\Http\ViewComposers\AddGoodFormComposer');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}