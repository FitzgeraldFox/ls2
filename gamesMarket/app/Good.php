<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{

    protected $fillable = ['name', 'cat_id', 'description', 'price', 'photo'];

    public function category()
    {
        return $this->belongsTo('App\Category', 'cat_id', 'id');
    }

}
