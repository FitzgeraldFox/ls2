<?php

namespace Traits;

use App\Good;
use App\PreOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

trait EmailTrait
{
    public function send($order)
    {
        $email_list = DB::table('email_settings')->select('*')->first();
        if (!empty($email_list)) {
            if ($email_list->send_admin_email == 1) {
                $email_list = explode(',', $email_list->email_list);
                $order_elems = [];
                $goods = PreOrder::select('good_id')->where('session_id', '=', session('id'))->get()->toArray();
                foreach ($goods as $good) {
                    $order_elems[] = Good::select('*')->where('id', '=', $good['good_id'])->get();
                }
                $sum = 0;
                foreach ($order_elems as $elem) {
                    foreach ($elem as $el) {
                        $sum += $el->price;
                    }
                }
                Mail::send('emails.send', [
                  'order' => $order,
                  'sum' => $sum,
                  'order_data' => $order_elems
                ], function ($message) use ($email_list) {
                    $message->from('cool-testmail.test@yandex.ru', 'Games Market');
                    $message->to($email_list);
                    $message->subject('Новый заказ на сайте Games Market');
                });
            }
        }
    }
}
