<?php

namespace App\Http\Controllers;

use App\Category;
use App\EmailSettings;
use App\Good;
use App\News;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ManagerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->middleware('checkAdmin');

        $elems_orders = [];
        $sum = [];

        $orders = Order::all()->sortByDesc('id')->each(
            function ($order, $order_key) use (&$sum, &$elems_orders) {
                $elems = explode(',', $order->order_data);
                $sum[$order_key] = 0;
                $elems_orders[$order_key] = Good::find($elems)->each(
                    function ($elem, $elem_index) use (&$sum, $order_key) {
                        $sum[$order_key] += $elem->price;
                    }
                );
            }
        );
        return view('manager', ['orders' => $orders, 'elems_orders' => $elems_orders, 'sum' => $sum]);
    }

    public function orders()
    {
        $this->middleware('checkAdmin');

        $elems_orders = [];
        $sum = [];

        $orders = Order::all()->sortByDesc('id')->each(
            function ($order, $order_key) use (&$sum, &$elems_orders) {
                $elems = explode(',', $order->order_data);
                $sum[$order_key] = 0;
                $elems_orders[$order_key] = Good::find($elems)->each(
                    function ($elem, $elem_index) use (&$sum, $order_key) {
                        $sum[$order_key] += $elem->price;
                    }
                );
            }
        );
        return view('manager.orders', ['orders' => $orders, 'elems_orders' => $elems_orders, 'sum' => $sum]);
    }

    public function categories()
    {
        $this->middleware('checkAdmin');

        $categories = Category::all()->toArray();
        krsort($categories);
        return view('manager.categories', ['categories' => $categories]);
    }

    public function categoriesTable()
    {
        $this->middleware('checkAdmin');

        $categories = Category::all()->toArray();
        krsort($categories);
        return view('manager.categoriesTable', ['categories' => $categories]);
    }

    public function newsTable()
    {
        $this->middleware('checkAdmin');

        $news = News::all()->toArray();
        krsort($news);
        return view('manager.newsTable', ['news_list' => $news]);
    }

    public function goodsTable()
    {
        $this->middleware('checkAdmin');

        $goods = Good::all()->sortByDesc('id');
        return view('manager.goodsTable', ['goods' => $goods]);
    }

    public function goods()
    {
        $this->middleware('checkAdmin');

        $goods = Good::all()->sortByDesc('id');
        return view('manager.goods', ['goods' => $goods]);
    }
    public function news()
    {
        $this->middleware('checkAdmin');

        $news = News::orderBy('id')->get();
        return view('manager.news', ['news' => $news]);
    }

    public function emailSettings()
    {
        $this->middleware('checkAdmin');

        $settings = EmailSettings::find(3);
        if (empty($settings)) {
            $settings = '';
        }
        return view('manager.email', ['settings' => $settings]);
    }

    public function setEmailSettings(Request $request)
    {
        $this->middleware('checkAdmin');

        switch ($request->send_admin_email) {
            case 'on':
                $send_mail = true;
                break;
            default:
                $send_mail = false;
        }
        $settings_row = EmailSettings::find(3);
        if (count($settings_row) == 0) {
            $newSettings = new EmailSettings();
            $newSettings->email_list = $request->email_list;
            $newSettings->send_admin_email = $send_mail;
            $newSettings->save();
        } else {
            $settings_row->update([
              'email_list'       => $request->email_list,
              'send_admin_email' => $send_mail
            ]);
        }
        return ['success' => 'true', 'message' => 'Настройки успешно обновлены'];
    }

    public function about()
    {
        $see_also = Good::select('*')->get()->shuffle()->slice(0, 3);
        return view('about', ['see_also' => $see_also]);
    }
}
