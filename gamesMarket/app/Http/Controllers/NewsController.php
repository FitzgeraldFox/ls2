<?php

namespace App\Http\Controllers;

use App\Good;
use App\News;
use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    public function index()
    {
        $news_list = News::select('photo', 'id', 'title', 'body', 'created_at')
          ->orderBy('id', 'DESC')
          ->paginate(6);
        return view('news', ['news_list' => $news_list]);
    }
    public function create(Request $request)
    {
        $this->middleware('checkAdmin');

        $this->validate($request, [
            'title' => 'required|max:100',
            'body' => 'required',
            'photo' => 'required|file'
        ]);

        $news = new News;
        $news->title = $request->title;
        $news->body = $request->body;
        $news->save();

        $request->file('photo')->store('public/news_img');
        $news->update(['photo' => '/storage/news_img/' . $request->file('photo')->hashName()]);

        return ['success' => 'true', 'message' => 'Новость успешно добавлена'];
    }

    public function show(News $news)
    {
        return view('news-item', ['news' => $news]);
    }
    public function update(Request $request, News $news)
    {
        $this->middleware('checkAdmin');

        $this->validate($request, [
            'title' => 'required|max:100',
            'body' => 'required',
            'photo' => 'required|file'
        ]);

        $request->file('photo')->store('public/news_img');
        $news->update(['photo' => '/storage/news_img/' . $request->file('photo')->hashName()]);

        $news->title = $request->input('title');
        $news->body  = $request->input('body');
        $news->save();
        return [
          'success' => 'true',
          'message' => 'Новость успешно отредактирована'
        ];
    }
    public function destroy(News $news)
    {
        $this->middleware('checkAdmin');
        Storage::delete(str_replace('storage', 'public', $news->photo));
        $news->delete();
        return [
          'success' => 'true',
          'message' => 'Новость успешно удалена'
        ];
    }
}
