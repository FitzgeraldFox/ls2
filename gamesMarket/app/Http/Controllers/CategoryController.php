<?php

namespace App\Http\Controllers;

use App\Category;
use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Rees\Sanitizer\Sanitizer;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::select('*')
            ->orderBy('id', 'DESC')
            ->paginate(6);
        return view('categories', ['categories' => $categories]);
    }
    public function create(Request $request)
    {
        $this->middleware('checkAdmin');

        $this->validate($request, [
          'name' => 'required|max:100'
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        return ['success' => 'true', 'message' => 'Категория успешно добавлена'];
    }

    public function show(Category $category)
    {
        return view(
            'category',
            [
              'category' => $category,
              'goods' => $category->goods()->paginate(6)
            ]
        );
    }
    public function update(Request $request, Category $category)
    {
        $this->middleware('checkAdmin');

        $this->validate($request, [
          'name' => 'required|max:100',
          'description' => 'required'
        ]);

        $category->update([
            'name'        => $request->input('name'),
            'description' => $request->input('description')
        ]);
        return [
            'success' => 'true',
            'message' => 'Категория успешно отредактирована'
        ];
    }
    public function destroy(Category $category)
    {
        $this->middleware('checkAdmin');

        $category->delete();
        return [
            'success' => 'true',
            'message' => 'Категория успешно удалена'
        ];
    }
}
