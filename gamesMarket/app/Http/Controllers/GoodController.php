<?php

namespace App\Http\Controllers;

use App\Good;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class GoodController extends Controller
{
    public function index()
    {
        $goods = Good::orderBy('id', 'DESC')->paginate(6);
        return view('index', ['goods' => $goods]);
    }
    public function create(Request $request)
    {
        $this->middleware('checkAdmin');

        $this->validate($request, [
            'name'   => 'required|max:100',
            'cat_id' => 'required|integer',
            'price'  => 'required|integer',
            'photo'  => 'required|file'
        ]);
        $good = new Good;
        $good->name = $request->name;
        $good->cat_id = $request->cat_id;
        $good->description = $request->description;
        $good->price = $request->price;
        $good->photo = '/storage/goods_img/' . $request->file('photo')->hashName();
        $good->save();

        $request->file('photo')->store('public/goods_img');

        return ['success' => 'true', 'message' => 'Товар успешно добавлен'];
    }

    public function lastGoods()
    {
        $goods = Good::orderBy('id', 'desc')->paginate(6);
        return [
          'success' => 'true',
          'message' => view(
              'parts.goods_container',
              ['goods' => $goods]
          )->render()
        ];
    }

    public function show(Good $good)
    {
        $see_also = Good::where('id', '!=', $good->id)->get()->shuffle()->slice(0, 3);
        return view('good', ['good' => $good, 'see_also' => $see_also]);
    }

    public function update(Request $request, Good $good)
    {
        $this->middleware('checkAdmin');

        $this->validate($request, [
          'name'     => 'required|max:100',
          'category' => 'required|integer',
          'price'    => 'required|integer',
          'photo'    => 'required|file'
        ]);

        $good->update([
          'name'        => $request->input('name'),
          'description' => $request->input('description'),
          'price'       => $request->input('price'),
          'photo'       => '/storage/goods_img/' . $request->file('photo')->hashName()
        ]);

        $request->file('photo')->store('public/goods_img');

        return [
          'success' => 'true',
          'message' => 'Товар успешно отредактирован'
        ];
    }
    public function destroy(Good $good)
    {
        $this->middleware('checkAdmin');

        Storage::delete(str_replace('storage', 'public', $good->photo));
        $good->delete();

        return [
          'success' => 'true',
          'message' => 'Товар успешно удалён'
        ];
    }
}
