<?php

namespace App\Http\Controllers;

use App\Good;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        if (empty($request->input('search'))) {
            return ['success' => 'false', 'message' => 'Вы ввели пустой поисковой запрос'];
        }
        switch ($request->input('type')) {
            case 'goods':
                $goods = Good::where('name', 'LIKE', "%{$request->input('search')}%")
                  ->orWhere('description', 'LIKE', "%{$request->input('search')}%")->get();
                if (count($goods) == 0) {
                    return ['success' => 'false', 'message' => 'По вашему запросу ничего не нашлось'];
                }
                return [
                    'success' => 'true',
                    view(
                        'parts.search_goods_container',
                        ['goods' => $goods, 'search' => true]
                    )->render()
                ];
                break;
            case 'news':
                $news = News::where('title', 'LIKE', "%{$request->input('search')}%")
                            ->orWhere('body', 'LIKE', "%{$request->input('search')}%")->get();
                if (count($news) == 0) {
                    return ['success' => 'false', 'message' => 'По вашему запросу ничего не нашлось'];
                }
                return [
                    'success' => 'true',
                    view(
                        'parts.search_news_container',
                        ['news' => $news, 'search' => true]
                    )->render()];
                break;
            case 'all':
                $goods = Good::where('name', 'LIKE', "%{$request->input('search')}%")
                  ->orWhere('description', 'LIKE', "%{$request->input('search')}%")->get();
                $news = News::where('title', 'LIKE', "%{$request->input('search')}%")
                  ->orWhere('body', 'LIKE', "%{$request->input('search')}%")->get();
                if ($goods->count() == 0 && $news->count() == 0) {
                    return ['success' => 'false', 'message' => 'По вашему запросу ничего не нашлось'];
                }
                if ($goods->count() != 0) {
                    $goods_html = view('parts.search_goods_container', ['goods' => $goods, 'search' => true])->render();
                } else {
                    $goods_html = '';
                }
                if ($news->count() != 0) {
                    $news_html = view('parts.search_news_container', ['news' => $news, 'search' => true])->render();
                } else {
                    $news_html = '';
                }

                return ['success' => 'true', 'goods' => $goods_html, 'news' => $news_html];
                break;
            default:
                return ['success' => 'false', 'message' => 'По вашему запросу ничего не нашлось'];
        }
    }
}
