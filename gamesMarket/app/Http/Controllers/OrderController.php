<?php
namespace App\Http\Controllers;

use App\Good;
use App\PreOrder;
use Traits\EmailTrait;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class OrderController extends Controller
{
    use EmailTrait;

    public function create(Request $request)
    {
        $this->validate($request, [
          'name'        => 'required|max:100',
          'email'       => 'required|email|max:100',
          'one-good-id' => 'integer'
        ]);

        $order = new Order();
        $order->name = htmlentities(strip_tags($request->name));
        $order->email = htmlentities(strip_tags($request->email));
        $one_good_id = $request->input('one-good-id');
        if (isset($one_good_id) && !empty($one_good_id)) {
            $order->order_data = $one_good_id;
        } else {
            $preOrders = PreOrder::select('good_id')
              ->where('session_id', '=', session('id'))->get()->toArray();

            $pre_orders_good_id = [];
            foreach ($preOrders as $preOrder) {
                $pre_orders_good_id[] = $preOrder['good_id'];
            }
            $order->order_data = implode(',', $pre_orders_good_id);
            $this->send($order);
            PreOrder::where('session_id', '=', session('id'))->delete();
        }
        $order->save();
        return ['success' => 'true', 'message' => 'Ваш заказ успешно оформлен'];
    }

    public function addToBasket(Request $request, Good $good)
    {
        if (!$request->session()->exists('id')) {
            session(['id' => uniqid('', true)]);
        }
        $preOrder = new PreOrder();
        $preOrder->session_id = session('id');
        $preOrder->good_id = $good->id;
        $preOrder->save();
        return ['success' => 'true', 'message' => 'Товар успешно добавлен в корзину'];
    }

    public function show(Request $request)
    {
        $goods = [];
        $preOrders = '';
        if ($request->session()->exists('id')) {
            $preOrders = PreOrder::select('*')->where('session_id', '=', session('id'))->paginate(6);
        }

        if (!empty($preOrders)) {
            foreach ($preOrders as $good_id) {
                $goods[] = Good::find($good_id);
            }
            $goods = paginateCollection(collect($goods), 6);
        } else {
            $goods = '';
        }
        return view('basket', ['goods' => $goods]);
    }
}
