<?php

namespace App\Http\ViewComposers;

use App\Category;
use App\News;
use Illuminate\View\View;

class SidebarComposer
{

    protected $categories = [];
    protected $last_news = [];

    public function __construct()
    {
        $categories = Category::select('name', 'id')
            ->orderBy('name')->get()
            ->toArray();
        $last_news = News::select('photo', 'id', 'title')
            ->orderBy('id', 'DESC')
            ->take(3)
            ->get()
            ->toArray();
        $this->categories = $categories;
        $this->last_news = $last_news;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(['categories' => $this->categories, 'last_news' => $this->last_news]);
    }
}