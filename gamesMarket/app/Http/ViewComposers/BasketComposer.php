<?php

namespace App\Http\ViewComposers;

use App\Good;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class BasketComposer
{
    protected $goods_sum;

    public function __construct()
    {
        if (!empty($_SESSION['goods'])) {
            $good_ids = $_SESSION['goods'];
            $goods = Good::find($good_ids);
            $this->goods_sum = $goods->sum('price');
        } else {
            $this->goods_sum = '';
        }
    }

    public function compose(View $view)
    {
        $view->with(['goods_sum' => $this->goods_sum]);
    }
}