<?php

namespace App\Http\ViewComposers;

use App\Category;
use App\Good;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ManagerEditGoodComposer
{
    protected $categories;

    public function __construct()
    {
        $this->categories = Category::all();
    }

    public function compose(View $view)
    {
        $view->with(['categories' => $this->categories]);
    }
}