<?php

namespace App\Http\ViewComposers;

use App\Good;
use Illuminate\View\View;

class RandomGoodComposer
{
    protected $random_good;

    public function __construct()
    {
        $this->random_good = Good::select('photo', 'id', 'name', 'price')
          ->inRandomOrder()
          ->take(1)
          ->get()
          ->toArray();
    }

    public function compose(View $view)
    {
        $view->with(['random_good' => $this->random_good]);
    }
}