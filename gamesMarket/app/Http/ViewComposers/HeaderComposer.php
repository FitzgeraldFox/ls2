<?php

namespace App\Http\ViewComposers;

use App\PreOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class HeaderComposer
{
    protected $goods_count;
    protected $is_admin;

    public function __construct()
    {
        if (!empty($_SESSION)) {
            $preOrders = PreOrder::select('*')->where('session_id', '=', $_SESSION['id'])->get();
            if (!empty($preOrders) && count($preOrders) > 0) {
                $this->goods_count = $preOrders->count();
            } else {
                $this->goods_count = 0;
            }
        } else {
            $this->goods_count = 0;
        }

        if (Auth::check() && Auth::user()->is_admin == 1) {
            $this->is_admin = true;
        } else {
            $this->is_admin = false;
        }
    }

    public function compose(View $view)
    {
        $view->with(['goods_count' => $this->goods_count, 'is_admin' => $this->is_admin]);
    }
}