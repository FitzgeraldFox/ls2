<?php

namespace App\Listeners;

use App\PreOrder;

session_start();

class UserEventSubscriber
{
    public function onUserLogout($event)
    {
        if (isset($_SESSION['id'])) {
            PreOrder::where('session_id', '=', $_SESSION['id'])->delete();
            unset($_SESSION['id']);
        }
        if (isset($_SESSION['goods'])) {
            unset($_SESSION['goods']);
        }
        session_destroy();
    }

    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );
    }

}