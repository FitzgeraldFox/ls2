<?php

Route::get('/', 'GoodController@index');

Route::get('/customers', 'CategoryController@create');

Route::get('/search', 'SearchController@search');

Route::get('/last_goods', 'GoodController@lastGoods');

Route::get('/add_to_basket/{good}', 'OrderController@addToBasket');

Route::get('/basket', 'OrderController@show');

Route::get('/about', 'ManagerController@about');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => '/manager'], function () {

        Route::get('/', [
          'as' => 'manager',
          'uses' => 'ManagerController@index'
        ]);

        Route::get('orders', [
          'as' => 'manager-orders',
          'uses' => 'ManagerController@orders'
        ]);

        Route::get('categories', [
          'as' => 'manager-categories',
          'uses' => 'ManagerController@categories'
        ]);

        Route::get('categories/table', [
          'as' => 'manager-categories-table',
          'uses' => 'ManagerController@categoriesTable'
        ]);

        Route::get('goods', [
          'as' => 'manager-goods',
          'uses' => 'ManagerController@goods'
        ]);

        Route::get('goods/table', [
          'as' => 'manager-goods-table',
          'uses' => 'ManagerController@goodsTable'
        ]);

        Route::get('news', [
          'as' => 'manager-news',
          'uses' => 'ManagerController@news'
        ]);

        Route::get('news/table', [
          'as' => 'manager-news-table',
          'uses' => 'ManagerController@newsTable'
        ]);

        Route::get('email', [
          'as' => 'manager-email',
          'uses' => 'ManagerController@emailSettings'
        ]);

        Route::get('set_email_settings', [
          'as' => 'manager-set_email_settings',
          'uses' => 'ManagerController@setEmailSettings'
        ]);
    });
});

Route::resource('orders', 'OrderController');

Route::resource('categories', 'CategoryController');

Route::resource('goods', 'GoodController');
Route::post('goods/create', 'GoodController@create');
Route::post('goods/{good}', 'GoodController@update');

Route::resource('news', 'NewsController');
Route::post('news/create', 'NewsController@create');
Route::post('news/{news}', 'NewsController@update');
