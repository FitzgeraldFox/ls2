<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\News::class, function (Faker\Generator $faker) {
    $photos = [
      '/img/cover/game-1.jpg',
      '/img/cover/game-3.jpg',
      '/img/cover/game-4.jpg',
      '/img/cover/game-5.jpg',
      '/img/cover/game-6.jpg',
      '/img/cover/game-7.jpg',
      '/img/cover/game-8.jpg',
      '/img/cover/game-9.jpg'
    ];
    return [
        'title' => $faker->title,
        'body'  => $faker->text(),
        'photo' => $photos[mt_rand(0, 7)]
    ];
});
