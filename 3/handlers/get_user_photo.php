<?php
error_reporting(-1);

require 'db_connect.php';

session_start();

$settings = parse_ini_file('../config.ini');

$dbh = db_connect();
$sth = $dbh->prepare($settings['get_user_data_by_email']);
$sth->bindParam('email', $_SESSION['email']);
$sth->execute();

try {
    $userData = $sth->fetchAll();
} catch (PDOException $e) {
    die(json_encode(['success' => 'false']));
}
if (count($userData) > 0) {
    if ($userData[0]['filename'] != 'NULL') {
        die(json_encode(["success" => 'true',
          "message" => "
                <tr>
                    <td>
                        {$userData[0]['filename']}
                    </td>
                    <td>
                        <img src='userFiles/" . $userData[0]['filename'] . "'>
                    </td>
                    <td>
                        <a href=# data-delete-file>Удалить аватарку пользователя</a>
                    </td>
                </tr>"]));
    } else {
        die(json_encode(['success' => 'false']));
    }
}