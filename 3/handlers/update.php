<?php
session_start();

require 'user.php';
require 'db_connect.php';

$settings = parse_ini_file('../config.ini');

error_reporting(-1);

$userData = filterUserData();
$dbh      = db_connect();

switch ($_POST['action']) {
    case 'update':
        updateUserInfo([
           'dbh'      => $dbh,
           'userData' => $userData
        ]);
        break;
}

