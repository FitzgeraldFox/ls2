<?php
$settings = parse_ini_file('../config.ini');
error_reporting(-1);
try {
    $dbh = new PDO($settings['dsn'], $settings['user'], $settings['password']);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    if (json_decode($check_user, true)['success'] == 'false') {
        die($check_user);
    } else {
        die('YES!');
    }
} catch (PDOException $e) {
    return '{"success": "false"}';
}