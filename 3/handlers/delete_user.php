<?php
session_start();

error_reporting(-1);

require 'db_connect.php';
require 'user.php';

$userData    = filterUserData();
$settings    = parse_ini_file('../config.ini');
$dbh         = db_connect();
$allUserData = getUserData([
    'dbh' => $dbh,
    'id'  => $userData['id']
]);

$has_user_in_db = $dbh->prepare($settings['has_user_in_db']);
$has_user_in_db->bindParam('id', $userData['id']);
$has_user_in_db->execute();
$user = $has_user_in_db->fetchAll();

if (count($user) > 0) {
    $sth = $dbh->prepare($settings['delete_user']);
    $sth->bindParam('id', $userData['id']);
    $sth->execute();
    if (is_file('../userFiles/' . $allUserData[0]['filename'])) {
        unlink('../userFiles/' . $allUserData[0]['filename']);
    }
    if ($allUserData[0]['email'] == $_SESSION['email']) {
        session_unset();
        die(json_encode(['success' => 'true', 'message' => $settings['userDeleteSuccess'], 'redirect' => 'true', 'auth_page' => $settings['auth_page']]));
    } else {
        die(json_encode(['success' => 'true', 'message' => $settings['userDeleteSuccess']]));
    }
} else {
    die(json_encode(['success' => 'false', 'message' => $settings['userDeleteError']]));
}
