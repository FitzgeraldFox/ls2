<?php
session_start();

error_reporting(-1);

require 'db_connect.php';

$settings = parse_ini_file('../config.ini');

$filename = trim($_POST['filename']);

if (is_file('../userFiles/' . $filename)) {
    unlink('../userFiles/' . $filename);
    $dbh = db_connect();
    $sth = $dbh->prepare($settings['delete_file']);
    $sth->bindParam('email', $_SESSION['email']);
    $sth->execute();
    die(json_encode(['success' => 'true', 'message' => $settings['userFileDeleteSuccess']]));
}
