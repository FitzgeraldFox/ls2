<?php
session_start();
$settings = parse_ini_file('../config.ini');
if (isset($_SESSION['email'])) {
    unset($_SESSION["email"]);

    session_destroy();
    return json_encode(['success' => 'true']);

}