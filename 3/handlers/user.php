<?php
function check_user($params)
{
    if (empty($params['dbh'])) {
        die(json_encode(['success' => 'false', 'message' => 'Ошибка сервера']));
    }
    try {
        switch ($params['type']) {
            case 'registration':
                $sql = $GLOBALS['settings']['check_user_reg'];
                $sth = $params['dbh']->prepare($sql);
                $sth->bindParam('email', $params['userData']['email']);
                $sth->execute();
                try {
                    $user_count = $sth->fetchAll();
                } catch (PDOException $e) {
                    die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['simpleRegError']]));
                }
                if (count($user_count) > 0) {
                    die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['regError']]));
                } else {
                    return true;
                }
                break;
            case "auth":
                $sql  = $GLOBALS['settings']['check_user_auth'];
                $sth  = $params['dbh']->prepare($sql);
                $sth->bindParam('email', $params['userData']['email']);
                $pass = crypt($params['userData']['password'], $GLOBALS['settings']['salt']);
                $sth->bindParam('password', $pass);
                $sth->execute();
                try {
                    $user_count = $sth->fetchAll();
                } catch (PDOException $e) {
                    die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['simpleAuthError']]));
                }
                if (count($user_count) > 0) {
                    $_SESSION['email'] = $params['userData']['email'];
                    die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['updateUser']]));
                } else {
                    die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['authError']]));
                }
                break;
        }
    } catch (PDOException $e) {
        return ['success' => false];
    }
}

function filterUserData()
{
    if (!filter_has_var(INPUT_POST, 'action')) {
        die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['filterError']]));
    }
    switch($_POST['action']){
        case 'registration':
            if (
                !filter_has_var(INPUT_POST, 'action') ||
                !filter_has_var(INPUT_POST, 'email') ||
                !filter_has_var(INPUT_POST, 'password') ||
                !filter_has_var(INPUT_POST, 'confirm-password')
            ) {
                die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['filterError']]));
            }
            $filter_args = [
                'action'           => FILTER_SANITIZE_STRING,
                'email'            => FILTER_VALIDATE_EMAIL,
                'password'         => FILTER_SANITIZE_STRING,
                'confirm-password' => FILTER_SANITIZE_STRING
            ];
            $userData = filter_input_array(INPUT_POST, $filter_args);
            if ($userData['password'] != $userData['confirm-password']) {
                die(json_encode(['success' => 'false', 'message' => 'Введите правильное подтверждение пароля']));
            }
            return $userData;
            break;
        case 'auth':
            if (
                !filter_has_var(INPUT_POST, 'email') ||
                !filter_has_var(INPUT_POST, 'password')
            ) {
                die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['filterError']]));
            }
            $filter_args = [
                'action'           => FILTER_SANITIZE_STRING,
                'email'            => FILTER_VALIDATE_EMAIL,
                'password'         => FILTER_SANITIZE_STRING
            ];
            return filter_input_array(INPUT_POST, $filter_args);
            break;
        case 'update':
            if (
                !filter_has_var(INPUT_POST, 'username') ||
                !filter_has_var(INPUT_POST, 'age') ||
                !filter_has_var(INPUT_POST, 'about')
            ) {
                die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['filterError']]));
            }
            $filter_args = [
                'username'    => FILTER_SANITIZE_STRING,
                'age'         => [
                    'filter'  => FILTER_VALIDATE_INT,
                    'options' => [
                        'min_range' => 0
                    ]
                ],
              'about'    => FILTER_SANITIZE_STRING,
              'action'   => FILTER_SANITIZE_STRING
            ];
            return filter_input_array(INPUT_POST, $filter_args);
            break;
        case 'delete_user':
            if (
            !filter_has_var(INPUT_POST, 'id')
            ) {
                die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['filterError']]));
            }
            $filter_args = [
              'id'       => FILTER_SANITIZE_STRING,
              'action'   => FILTER_SANITIZE_STRING
            ];
            return filter_input_array(INPUT_POST, $filter_args);
            break;
        default:
            die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['filterError']]));
    }
};

function create_user($params)
{
    if (empty($params['dbh'])) {
        return false;
    }

    $sql  = $GLOBALS['settings']['create_user'];
    $sth  = $params['dbh']->prepare($sql);
    $sth->bindParam('email', $params['userData']['email']);
    $pass = crypt($params['userData']['password'], $GLOBALS['settings']['salt']);
    $sth->bindParam('password', $pass);
    if ($sth->execute()) {
        $_SESSION['email'] = $params['userData']['email'];
        die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['updateUser']]));
    } else {
        die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['userCreateError']]));
    };
}

function updateUserInfo($params)
{
    $wrongFileExt  = false;
    $sql           = $GLOBALS['settings']['update_user_info'];
    $sth           = $params['dbh']->prepare($sql);

    if (empty($params['userData']['username'])) {
        $params['userData']['username'] = 'NULL';
    }
    if (empty($params['userData']['age'])) {
        $params['userData']['age'] = '0';
    }
    if (empty($params['userData']['about'])) {
        $params['userData']['about'] = 'NULL';
    }

    $sth->bindParam('email', $_SESSION['email']);
    $sth->bindParam('username', $params['userData']['username']);
    $sth->bindParam('age', $params['userData']['age']);
    $sth->bindParam('about', $params['userData']['about']);
    $params['userData']['filename'] = 'NULL';
    $sth->bindParam('filename', $params['userData']['filename']);
    if ($sth->execute()) {
        $sql2 = $GLOBALS['settings']['get_user_data_by_email'];
        $sth2 = $params['dbh']->prepare($sql2);
        $sth2->bindParam('email', $_SESSION['email']);
        $sth2->execute();
        $userData = $sth2->fetchAll();

        if ( empty($_FILES) === false) {
            $file = empty($_FILES['file']) ? null : $_FILES['file'];
            if ($file !== null) {

                if ($file["error"] == UPLOAD_ERR_OK) {

                    $ext      = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
                    $img_exts = ['jpeg', 'jpg', 'png', 'gif'];

                    if ( in_array($ext, $img_exts) === false) {
                        $wrongFileExt = true;
                    } else {
                        if ($_FILES['file']['size'] > 320000000) {
                            die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['largeFile']]));
                        }
                        $filename = $userData[0]['id'] . '.' . $ext;
                        if (!is_dir('../userFiles')) {
                            mkdir('../userFiles');
                        } else {
                            if (is_file("../userFiles/$filename")) {
                                unlink("../userFiles/$filename");
                            }
                        }

                        $sth->bindParam('filename', $filename);
                        $sth->execute();
                        resizeFile($file, "../userFiles/$filename");
                    }
                }
            }
        }
        if ($wrongFileExt) {
            die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['wrongFileExt']]));
        }
        die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['userUpdateSuccess']]));
    } else {
        die(json_encode(['success' => 'false', 'message' => $GLOBALS['settings']['userUpdateError']]));
    }
}

function getUserData($params)
{
    if (!empty($params['id'])) {
        $sql = $GLOBALS['settings']['get_user_data_by_id'];
        $sth = $params['dbh']->prepare($sql);
        $sth->bindParam('id', $params['id']);
    } else {
        $sql = $GLOBALS['settings']['get_user_data_by_email'];
        $sth = $params['dbh']->prepare($sql);
        $sth->bindParam('email', $_SESSION['email']);
    }
    $sth->execute();
    try {
        $user = $sth->fetchAll();
    } catch (PDOException $e) {
        return false;
    }
    return $user;
}

function getAllUsersData($params)
{
    $sql = $GLOBALS['settings']['get_all_user_data'];
    $sth = $params['dbh']->prepare($sql);
    $sth->execute();
    try {
        $user = $sth->fetchAll();
    } catch (PDOException $e) {
        return false;
    }
    return $user;
}

function getFileList($params)
{
    $sql = $GLOBALS['settings']['get_file_list'];
    $sth = $params['dbh']->prepare($sql);
    $sth->execute();
    try {
        $filenames = $sth->fetchAll();
    } catch (PDOException $e) {
        return false;
    }
    return $filenames;
}

function resizeFile($file, $filepath)
{
    $src = imagecreatefromjpeg($file['tmp_name']);
    $w_src = imagesx($src);
    $h_src = imagesy($src);
    $max_w = 400;
    $max_h = 800;
    if (
      $w_src >= $max_w ||
      $h_src >= $max_h
    ) {
        $ratio  = $w_src/$max_w;
        $w_dest = round($w_src/$ratio);
        $h_dest = round($h_src/$ratio);
        $dest   = imagecreatetruecolor($w_dest,$h_dest);
        imagecopyresized($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
        switch ($file['type']) {
            case 'image/jpeg':
                imagejpeg($dest, $filepath, 100);
                break;
            case 'image/png':
                imagepng($dest, $filepath, 100);
                break;
            case 'image/gif':
                imagegif($dest, $filepath, 100);
                break;
        }
    } else {
        move_uploaded_file($file['tmp_name'], $filepath);
    }
}