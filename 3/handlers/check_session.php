<?php
session_start();

$settings = parse_ini_file('../config.ini');

switch ($_POST['action']) {
    case 'start':
        if (!isset($_SESSION['email'])) {
            die(json_encode(['redirect' => 'true', 'auth_page' => $settings['auth_page']]));
        }
        break;
    case 'end':
        if (isset($_SESSION['email'])) {
            unset($_SESSION["email"]);
        }
        die(json_encode(['redirect' => 'true', 'auth_page' => $settings['auth_page']]));
        break;
}
