<?php
session_start();

require 'user.php';
require 'db_connect.php';

error_reporting(-1);

$settings = parse_ini_file('../config.ini');
$userData = filterUserData();
$dbh = db_connect();

switch ($userData['action']) {
    case 'registration':
        $check_user = check_user([
            'type'     => 'registration',
            'dbh'      => $dbh,
            'userData' => $userData
        ]);
        create_user([
            'dbh'      => $dbh,
            'userData' => $userData
        ]);
        break;
    case 'auth':
        check_user([
            'type'     => 'auth',
            'dbh'      =>  $dbh,
            'userData' => $userData
        ]);
        break;
}