<?php
error_reporting(-1);

function db_connect()
{
    try {
        $dbh = new PDO(
            $GLOBALS['settings']['dsn'],
            $GLOBALS['settings']['user'],
            $GLOBALS['settings']['password']
        );
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbh;
    } catch (PDOException $e) {
        die(json_encode([$e->getMessage()]));
    }
}