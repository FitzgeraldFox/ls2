<?php
session_start();
require 'handlers/user.php';
require 'handlers/db_connect.php';
$settings = parse_ini_file('config.ini');
if (!isset($_SESSION['email'])) {
    header("Location: {$settings['auth_page']}");
}
$dbh = db_connect();
$users = getAllUsersData([
  'dbh' => $dbh
]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.brighttheme.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.css" />
    <link rel="stylesheet" href="style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Личный кабинет</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="list.php">Список пользователей</a></li>
                <li><a href="filelist.php">Список файлов</a></li>
                <li id="exitLK">
                    <a href="#">Выйти из личного кабинета</a>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<table class="table table-bordered" id="userListTable"
    <?php
    if (count($users) < 0) {
        echo 'style="display: none"';
        $has_users = false;
    } else {
        $has_users = true;
    }
    ?>
>
    <thead>
        <th>Email</th>
        <th>Имя</th>
        <th>Возраст</th>
        <th>Описание</th>
        <th>Фотография</th>
        <th>Действия</th>
    </thead>
    <tbody>
    <?php
    if ($has_users) {
        foreach ($users as $user) {
            echo "
            <tr data-user-id={$user['id']}>
                <td>{$user['email']}</td>";
            if (!empty($user['name'])) {
                echo "<td>{$user['name']}</td>";
            } else {
                echo '<td>Нет имени</td>';
            };
            if (!empty($user['age'])) {
                echo "<td>{$user['age']}</td>";
            } else {
                echo '<td>Нет возраста</td>';
            };
            if (!empty($user['about'])) {
                echo "<td>{$user['about']}</td>";
            } else {
                echo '<td>Нет описания</td>';
            };
            if (is_file("userFiles/{$user['filename']}")) {
                echo "<td><img src=userFiles/{$user['filename']}></td>";
            } else {
                echo "<td>Нет аватарки</td>";
            }
            echo "
                <td>
                    <a href='#' data-delete-user>Удалить пользователя</a>
                </td>
            </tr>";
        }
    } else {
        echo '<tr><td colspan="6">Список пользователей пуст</td></tr>';
    }
    ?>
    </tbody>
</table>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.js"></script>
<script src="js/index.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>