<?php
session_start();

require 'handlers/user.php';
require 'handlers/db_connect.php';

$settings = parse_ini_file('config.ini');

if (!isset($_SESSION['email'])) {
    header("Location: {$settings['auth_page']}");
}
$dbh      = db_connect();
$userData = getUserData([
    'dbh' => $dbh
]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.brighttheme.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.css" />
    <link rel="stylesheet" href="style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Личный кабинет</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="list.php">Список пользователей</a></li>
                    <li><a href="filelist.php">Список файлов</a></li>
                    <li id="exitLK">
                        <a href="#">Выйти из личного кабинета</a>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <form enctype="multipart/form-data" id="userInfoForm" class="form">
        <div class="form-group">
            <label>
                <p>Ваше имя:</p>
                <input type="text" name="username" value="<?=$userData ? $userData[0]['name'] ? $userData[0]['name'] : '' : ''?>" class="form-control">
            </label>
        </div>
        <div class="form-group">
            <label>
                <p>Ваш возраст:</p>
                <input type="number" name="age" min="0" value="<?=$userData ? $userData[0]['age'] ? $userData[0]['age'] : '' : ''?>" class="form-control">
            </label>
        </div>
        <div class="form-group">
            <label>
                <p>Пару строк о себе:</p>
                <textarea name="about" class="form-control"><?=$userData ? $userData[0]['about'] ? $userData[0]['about'] : '' : ''?></textarea>
            </label>
        </div>
        <div class="form-group">
            <label>
                <p>Выберите аватарку:</p>
                <input type="file" name="file" placeholder="Прикрепить файл" accept="image/jpeg,image/png,image/gif">
            </label>
        </div>
        <button type="submit" class="submitButton">Изменить данные</button>
    </form>
    <table class="table table-bordered" id="userFileTable"
        <?php
        if ($userData) {
          if (!is_file('userFiles/' . $userData[0]['filename'])) {
              echo 'style="display: none"';
              $has_file = false;
          } else {
              $has_file = true;
          }
        } else {
          echo 'style="display: none"';
          $has_file = false;
        }
        ?>
    >
        <thead>
            <th>Название файла</th>
            <th>Фотография</th>
            <th>Действия</th>
        </thead>
        <tbody>
            <?php
            if ($has_file) {
                echo "<tr>
                    <td>{$userData[0]['filename']}</td>
                    <td><img src=userFiles/{$userData[0]['filename']} alt=''></td>
                    <td>
                        <a href='#' data-delete-file>Удалить аватарку пользователя</a>
                    </td>
                </tr>";
            }
            ?>
        </tbody>
    </table>
<script
src="https://code.jquery.com/jquery-2.2.4.min.js"
integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.js"></script>
<script src="js/index.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>