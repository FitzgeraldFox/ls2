$(document).ready(function () {
    $('body').on('submit', '#authForm', function(e){
        e.preventDefault();
        var userData = '';
        switch($(this).attr('data-form-type')) {
            case 'auth':
                userData = $(this).serialize();
                userData += '&action=auth';
                $.ajax({
                    url: 'handlers/register.php',
                    type: 'POST',
                    data: userData,
                    dataType: 'json',
                    success: function (response) {
                        if(response['success'] == 'true'){
                            window.location.pathname = response['message'];
                        } else {
                            new PNotify({
                                title: 'Ошибка',
                                type: 'error',
                                text: response['message']
                            })
                        }
                    }
                });
                break;
            case 'registration':
                if($(this).find('input[name=password]').val() != $(this).find('input[name=confirm-password]').val()) {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: 'Введите правильное подтверждение пароля'
                    })
                } else {
                    userData = $(this).serialize();
                    userData += '&action=registration';
                    $.ajax({
                        url: 'handlers/register.php',
                        type: 'POST',
                        data: userData,
                        dataType: 'json',
                        success: function (response) {
                            if(response['success'] == 'true'){
                                window.location.pathname = response['updateUser'];
                            } else {
                                new PNotify({
                                    title: 'Ошибка',
                                    type: 'error',
                                    text: response['message']
                                })
                            }
                        }
                    });
                }
                break;
        }
    });
    $('body').on('submit', '#userInfoForm', function(e) {
        e.preventDefault();
        var form = document.forms.userInfoForm;
        var fd = new FormData(form);
        fd.append('action', 'update');
        $.ajax({
            url: 'handlers/update.php',
            type: 'POST',
            data: fd,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (response) {
                if(response['success'] == 'true'){
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $.ajax({
                        url: 'handlers/get_user_photo.php',
                        type: 'POST',
                        dataType: 'json',
                        success: function (response) {
                            if (response['success'] == 'true') {
                                $('#userFileTable tbody').html(response['message']);
                                $('#userFileTable').fadeIn();
                            }
                        },
                        error: function() {
                            new PNotify({
                                title: 'Ошибка',
                                type: 'error',
                                text: 'Ошибка сервера'
                            })
                        }
                    })
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        });
    });

    $('body').on('click', '#exitLK', function(){
        $.ajax({
            url: 'handlers/check_session.php',
            type: 'POST',
            data: {'action': 'end'},
            dataType: 'json',
            success: function(response){
                if (response['redirect'] == 'true') {
                    window.location.pathname = response['auth_page'];
                }
            }
        })
    });

    $('body').on('click', '[data-delete-file]', function(){
        var self = $(this);
        $.ajax({
            url: 'handlers/delete_file.php',
            type: 'POST',
            data: {'filename': $(this).closest('tr').find('td:first').html()},
            dataType: 'json',
            success: function(response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    self.closest('tr').fadeOut(200);
                    setTimeout(function(){
                        self.closest('tr').remove();
                        if($('#userFileTable tbody tr').length == 0) {
                            $('#userFileTable').fadeOut(200);
                            setTimeout(function(){
                                $('#userFileTable').hide();
                            }, 200);
                        }
                    }, 200);
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('click', '[data-delete-user]', function(){
        var self = $(this);
        $.ajax({
            url: 'handlers/delete_user.php',
            type: 'POST',
            data: {'id': self.closest('tr').attr('data-user-id'), 'action': 'delete_user'},
            dataType: 'json',
            success: function(response) {
                if (response['redirect'] == 'true') {
                    window.location.pathname = response['auth_page'];
                }
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    self.closest('tr').fadeOut(200);
                    setTimeout(function(){
                        self.closest('tr').remove();
                        if($('#userListTable tbody tr').length == 0) {
                            $('#userListTable').fadeOut(200);
                            setTimeout(function(){
                                $('#userListTable').hide();
                            }, 200);
                        }
                    }, 200);
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    })
});

