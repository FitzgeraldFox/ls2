<?php
namespace Interfaces;

interface Controller
{
    public static function create($params);

    public static function read($mode);

    public static function update($params);

    public static function delete($params);
}