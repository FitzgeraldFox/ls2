<?php
function getSettings()
{
    return [
        'API_VERSION'                    => 'v1',
        'DRIVER'                         => 'mysql',
        'HOST'                           => 'localhost',
        'DATABASE'                       => 'ls7',
        'USERNAME'                       => 'root',
        'PASSWORD'                       => '',
        'CHARSET'                        => 'utf8',
        'COLLATION'                      => 'utf8_unicode_ci',
        'PREFIX'                         => '',
        'CategoryCreate'                 => 'Категория товаров успешно создана',
        'GoodCreate'                     => 'Товар успешно создан',
        'GoodCreateError'                => 'Ошибка при создании товара',
        'CategoryUpdate'                 => 'Категория успешно обновлена',
        'CategoryUpdateError'            => 'Ошибка при обновлении категории',
        'GoodUpdate'                     => 'Товар успешно обновлен',
        'GoodUpdateError'                => 'Ошибка при обновлении товара',
        'CategoryDelete'                 => 'Категория успешно удалена',
        'CategoryDeleteError'            => 'Ошибка при удалении категории',
        'GoodDelete'                     => 'Товар успешно удален',
        'GoodDeleteError'                => 'Ошибка при удалении товара',
        'CategoryModelNotFoundException' => 'Запрашиваемая категория не найдена',
        'GoodModelNotFoundException'     => 'Запрашиваемый товар не найден',
        'UndefinedPath'                  => 'Неверный путь',
        'UndefinedMethod'                => 'Неверный метод',
        'FilterError'                    => 'Ошибка валидации данных'
    ];
}