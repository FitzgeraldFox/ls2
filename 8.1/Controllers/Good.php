<?php
namespace Controllers;

require_once 'Interfaces/Controller.php';
require_once 'Models/Good.php';
require_once 'Validators/Validator.php';
require_once 'Validators/ValidatorHandler.php';
require_once 'Validators/ParseFormData.php';

use Models\Good as GoodModel;
use Interfaces\Controller;
use Validators\Validator;
use Validators\ValidatorHandler;
use Validators\ParseFormData;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Good implements Controller
{
    public static function create($params = [])
    {
        $validator = new ValidatorHandler('gump');
        $validation_rules = [
          'good_id' => 'required|integer|min_numeric,1',
          'name'    => 'required',
          'amount'  => 'required|integer|min_numeric,1',
          'price'   => 'required|integer|min_numeric,1',
          'options' => 'required|valid_json_string',
          'cat_id'  => 'required|integer|min_numeric,1'
        ];
        $filter_rules = [
          'good_id' => 'trim|sanitize_numbers',
          'name'    => 'trim|sanitize_string',
          'amount'  => 'trim|sanitize_numbers',
          'price'   => 'trim|sanitize_numbers',
          'options' => 'trim',
          'cat_id'  => 'trim|sanitize_numbers'
        ];

        $filtered_data = Validator::filter($validator, $validation_rules, $filter_rules, $_POST);
        try {
            $model = new GoodModel;
            $model->good_id = $filtered_data['good_id'];
            $model->name    = $filtered_data['name'];
            $model->amount  = $filtered_data['amount'];
            $model->price   = $filtered_data['price'];
            $model->options = $filtered_data['options'];
            $model->cat_id  = $filtered_data['cat_id'];
            $model->save();
        } catch (\Exception $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $e->getMessage()
            ]));
        }
        die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['GoodCreate']]));
    }

    public static function read($mode)
    {
        $validator = new ValidatorHandler('gump');

        switch ($mode) {
            case 'getAllGoods':
                return GoodModel::all()->toJson();
                break;
            case 'getGoodById':
                try {
                    $id = explode('/', explode('good/', $_SERVER['REQUEST_URI'])[1])[0];
                    $validation_rules = [
                      'id' => 'required|integer|min_numeric,1'
                    ];
                    $filter_rules = [
                      'id' => 'trim|sanitize_numbers'
                    ];
                    $filtered_data = Validator::filter($validator, $validation_rules, $filter_rules, [
                      'id' => $id
                    ]);
                    die(GoodModel::findOrFail($filtered_data['id'])->toJson());
                } catch (ModelNotFoundException $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['GoodModelNotFoundException']
                    ]));
                }
                break;
        }
    }

    public static function update($params = [])
    {
        $validator = new ValidatorHandler('gump');

        $validation_rules = [
          'id'      => 'required|integer|min_numeric,1',
          'good_id' => 'required|integer|min_numeric,1',
          'name'    => 'required',
          'amount'  => 'required|integer|min_numeric,1',
          'price'   => 'required|integer|min_numeric,1',
          'options' => 'required|valid_json_string',
          'cat_id'  => 'required|integer|min_numeric,1'
        ];
        $filter_rules = [
          'id'      => 'trim|sanitize_numbers',
          'good_id' => 'trim|sanitize_numbers',
          'name'    => 'trim|sanitize_string',
          'amount'  => 'trim|sanitize_numbers',
          'price'   => 'trim|sanitize_numbers',
          'options' => 'trim',
          'cat_id'  => 'trim|sanitize_numbers'
        ];

        $_PUT = ParseFormData::parseRawHttpRequest();
        $_PUT['id'] = explode('/', explode('good/', $_SERVER['REQUEST_URI'])[1])[0];
        try {
            $filtered_data = Validator::filter($validator, $validation_rules, $filter_rules, $_PUT);
            GoodModel::where('id', '=', $filtered_data['id'])->update([
              'name'      => $filtered_data['name'],
              'amount'    => $filtered_data['amount'],
              'price'     => $filtered_data['price'],
              'options'   => $filtered_data['options'],
              'cat_id'    => $filtered_data['cat_id']
            ]);
        } catch (ModelNotFoundException $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $GLOBALS['settings']['GoodModelNotFoundException']
            ]));
        } catch (\Exception $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $e->getMessage()
            ]));
        }
        die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['GoodUpdate']]));
    }

    public static function delete($params = [])
    {
        $validator = new ValidatorHandler('gump');

        $validation_rules =[
          'id' => 'required|integer|min_numeric,1'
        ];
        $filter_rules = [
          'id' => 'trim|sanitize_numbers'
        ];

        $_DELETE['id'] = explode('/', explode($validator, $validation_rules, $filter_rules, $_SERVER['REQUEST_URI'])[1])[0];
        try {
            $filtered_data = Validator::filter('deleteGood', $_DELETE);
            GoodModel::where('id', '=', $filtered_data['id'])->delete();
        } catch (ModelNotFoundException $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $GLOBALS['settings']['GoodModelNotFoundException']
            ]));
        } catch (\Exception $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $e->getMessage()
            ]));
        }
        die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['GoodDelete']]));
    }
}