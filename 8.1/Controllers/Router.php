<?php
namespace Controllers;

require_once 'Category.php';
require_once 'Good.php';
require_once 'Validators/Validator.php';

class Router
{
    public static function route()
    {
        $GLOBALS['settings'] = getSettings();
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                switch ($_SERVER['REQUEST_URI']) {
                    case "/api/{$GLOBALS['settings']['API_VERSION']}/categories":
                        die(Category::read('getAllCats'));
                        break;
                    case "/api/{$GLOBALS['settings']['API_VERSION']}/goods":
                        die(Good::read('getAllGoods'));
                        break;
                    case (
                        preg_match(
                            "/\/api\/{$GLOBALS['settings']['API_VERSION']}\/category\/\d/",
                            $_SERVER['REQUEST_URI']
                        ) ? true : false):
                        Category::read('getCatById');
                        break;
                    case (
                        preg_match(
                            "/\/api\/{$GLOBALS['settings']['API_VERSION']}\/good\/\d/",
                            $_SERVER['REQUEST_URI']
                        ) ? true : false):
                        Good::read('getGoodById');
                        break;
                    case '/':
                        header('Location: /index.html');
                        break;
                    default:
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['UndefinedPath']
                        ]));
                }
                break;
            case 'POST':
                switch ($_SERVER['REQUEST_URI']) {
                    case (
                        preg_match(
                            "/\/api\/{$GLOBALS['settings']['API_VERSION']}\/category/",
                            $_SERVER['REQUEST_URI']
                        ) ? true : false):
                        Category::create();
                        break;
                    case (
                        preg_match(
                            "/\/api\/{$GLOBALS['settings']['API_VERSION']}\/good/",
                            $_SERVER['REQUEST_URI']
                        ) ? true : false):
                        Good::create();
                        break;
                    default:
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['UndefinedPath']
                        ]));
                }
                break;
            case 'PUT':
                switch ($_SERVER['REQUEST_URI']) {
                    case (
                        preg_match(
                            "/\/api\/{$GLOBALS['settings']['API_VERSION']}\/category\/\d/",
                            $_SERVER['REQUEST_URI']
                        ) ? true : false):
                        Category::update();
                        break;
                    case (
                        preg_match(
                            "/\/api\/{$GLOBALS['settings']['API_VERSION']}\/good\/\d/",
                            $_SERVER['REQUEST_URI']
                        ) ? true : false):
                        Good::update();
                        break;
                    default:
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['UndefinedPath']
                        ]));
                }
                break;
            case 'DELETE':
                switch ($_SERVER['REQUEST_URI']) {
                    case (
                        preg_match(
                            "/\/api\/{$GLOBALS['settings']['API_VERSION']}\/category\/\d/",
                            $_SERVER['REQUEST_URI']
                        ) ? true : false):
                        Category::delete();
                        break;
                    case (
                        preg_match(
                            "/\/api\/{$GLOBALS['settings']['API_VERSION']}\/good\/\d/",
                            $_SERVER['REQUEST_URI']
                        ) ? true : false):
                        Good::delete();
                        break;
                    default:
                        die(json_encode([
                          'success' => 'false',
                          'message' => $GLOBALS['settings']['UndefinedPath']
                        ]));
                }
                break;
            default:
                die(json_encode([
                  'success' => 'false',
                  'message' => $GLOBALS['settings']['UndefinedMethod']
                ]));
        }
    }
}