<?php
namespace Controllers;

require_once 'Interfaces/Controller.php';
require_once 'Models/Category.php';
require_once 'Validators/Validator.php';
require_once 'Validators/ValidatorHandler.php';
require_once 'Validators/ParseFormData.php';

use Models\Category as CategoryModel;
use Interfaces\Controller;
use Validators\Validator;
use Validators\ValidatorHandler;
use Validators\ParseFormData;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Category implements Controller
{
    public static function create($params = [])
    {
        $validator = new ValidatorHandler('gump');
        $validation_rules = [
          'name'        => 'required',
          'description' => 'required'
        ];
        $filter_rules = [
          'name'        => 'trim|sanitize_string',
          'description' => 'trim|sanitize_string',
        ];
        $filtered_data = Validator::filter($validator, $validation_rules, $filter_rules, $_POST);
        $model = new CategoryModel;
        $model->name         = $filtered_data['name'];
        $model->description  = $filtered_data['description'];
        $model->save();
        die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['CategoryCreate']]));
    }

    public static function read($mode)
    {
        $validator = new ValidatorHandler('gump');
        switch ($mode) {
            case 'getAllCats':
                return CategoryModel::all()->toJson();
                break;
            case 'getCatById':
                try {
                    $id = intval(explode('/', explode('category/', $_SERVER['REQUEST_URI'])[1])[0]);
                    $validation_rules = [
                      'id' => 'required|integer|min_numeric,1'
                    ];
                    $filter_rules = [
                      'id' => 'trim|sanitize_numbers'
                    ];
                    $filtered_data = Validator::filter($validator, $validation_rules, $filter_rules, [
                      'id' => $id
                    ]);
                    die(CategoryModel::findOrFail($filtered_data['id'])->toJson());
                } catch (ModelNotFoundException $e) {
                    die(json_encode([
                      'success' => 'false',
                      'message' => $GLOBALS['settings']['CategoryModelNotFoundException']
                    ]));
                }
                break;
        }
    }

    public static function update($params = [])
    {
        $validator = new ValidatorHandler('gump');
        $validation_rules = [
          'id'          => 'required|integer|min_numeric,1',
          'name'        => 'required',
          'description' => 'required'
        ];
        $filter_rules = [
          'id'          => 'trim|sanitize_numbers',
          'name'        => 'trim|sanitize_string',
          'description' => 'trim|sanitize_string',
        ];
        $_PUT = ParseFormData::parseRawHttpRequest();
        $_PUT['id'] = explode('/', explode('category/', $_SERVER['REQUEST_URI'])[1])[0];
        try {
            $filtered_data = Validator::filter($validator, $validation_rules, $filter_rules, $_PUT);
            CategoryModel::where('id', '=', $filtered_data['id'])->update([
              'name'        => $filtered_data['name'],
              'description' => $filtered_data['description']
            ]);
        } catch (ModelNotFoundException $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $GLOBALS['settings']['CategoryModelNotFoundException']
            ]));
        } catch (\Exception $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $e->getMessage()
            ]));
        }
        die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['CategoryUpdate']]));
    }

    public static function delete($params = [])
    {
        $validator = new ValidatorHandler('gump');
        $validation_rules =[
          'id' => 'required|integer|min_numeric,1'
        ];
        $filter_rules = [
          'id' => 'trim|sanitize_numbers'
        ];
        $_DELETE['id'] = explode('/', explode('category/', $_SERVER['REQUEST_URI'])[1])[0];
        try {
            $filtered_data = Validator::filter($validator, $validation_rules, $filter_rules, $_DELETE);
            CategoryModel::where('id', '=', $filtered_data['id'])->delete();
        } catch (ModelNotFoundException $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $GLOBALS['settings']['CategoryModelNotFoundException']
            ]));
        } catch (\Exception $e) {
            die(json_encode([
              'success' => 'false',
              'message' => $e->getMessage()
            ]));
        }
        die(json_encode(['success' => 'true', 'message' => $GLOBALS['settings']['CategoryDelete']]));
    }
}