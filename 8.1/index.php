<?php
require_once 'Controllers/Router.php';

require "vendor/autoload.php";

use Controllers\Router;

Router::route();
