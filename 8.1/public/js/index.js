$(document).ready(function () {

    updateCats(true);
    updateGoods(true);

    $('body').on('click', '#getAllCats', function (e) {
        e.preventDefault();
        updateCats();
    });

    $('body').on('click', '#getAllGoods', function (e) {
        e.preventDefault();
        updateGoods();
    });

    $('body').on('click', '[data-type]', function (e) {
        e.preventDefault();
        switch ($(this).attr('data-type')) {
            case 'getCatById':
                $('#getCatByIdForm').show();
                $('#getCatByIdForm').arcticmodal({
                    afterClose: function(data, el) {
                        $('#getCatByIdForm').hide();
                        updateCats();
                    }
                });
                break;
            case 'getGoodById':
                $('#getGoodByIdForm').show();
                $('#getGoodByIdForm').arcticmodal({
                    afterClose: function(data, el) {
                        $('#getGoodByIdForm').hide();
                        updateGoods();
                    }
                });
                break;
            case 'addCat':
                $('#addCatForm').show();
                $('#addCatForm').arcticmodal({
                    afterClose: function(data, el) {
                        $('#addCatForm').hide();
                        updateCats();
                    }
                });
                break;
            case 'editCat':
                var tr = $(this).closest('tr');
                $('#editCatForm [name="id"]').val($(tr).find('td:nth-child(1)').text());
                $('#editCatForm [name="name"]').val($(tr).find('td:nth-child(2)').text());
                $('#editCatForm [name="description"]').val($(tr).find('td:nth-child(3)').text());
                $('#editCatForm').show();
                $('#editCatForm').arcticmodal({
                    afterClose: function(data, el) {
                        $('#editCatForm').hide();
                        updateCats();
                    }
                });
                break;
            case 'deleteCat':
                $('#deleteCatForm').show();
                $('#deleteCatForm input[name=id]').val($(this).closest('tr').find('td:first-child').text());
                $('#deleteCatForm').arcticmodal({
                    afterClose: function(data, el) {
                        $('#deleteCatForm').hide();
                        updateCats();
                        updateGoods()
                    }
                });
                break;
            case 'addGood':
                $('#addGoodForm').show();
                $('#addGoodForm').arcticmodal({
                    afterClose: function(data, el) {
                        $('#addGoodForm').hide();
                        updateGoods();
                    }
                });
                break;
            case 'editGood':
                var tr = $(this).closest('tr');
                $('#editGoodForm [name="id"]').val($(tr).find('td:nth-child(1)').text());
                $('#editGoodForm [name="good_id"]').val($(tr).find('td:nth-child(2)').text());
                $('#editGoodForm [name="name"]').val($(tr).find('td:nth-child(3)').text());
                $('#editGoodForm [name="amount"]').val($(tr).find('td:nth-child(4)').text());
                $('#editGoodForm [name="price"]').val($(tr).find('td:nth-child(5)').text());
                $('#editGoodForm [name="options"]').val($(tr).find('td:nth-child(6)').text());
                $('#editGoodForm [name="cat_id"]').val($(tr).find('td:nth-child(7)').text());
                $('#editGoodForm').show();
                $('#editGoodForm').arcticmodal({
                    afterClose: function(data, el) {
                        $('#editGoodForm').hide();
                        updateGoods();
                    }
                });
                break;
            case 'deleteGood':
                $('#deleteGoodForm').show();
                $('#deleteGoodForm input[name=id]').val($(this).closest('tr').find('td:first-child').text());
                $('#deleteGoodForm').arcticmodal({
                    afterClose: function(data, el) {
                        $('#deleteGoodForm').hide();
                        updateGoods();
                    }
                });
                break;
        }
    });

    $('body').on('click', '[data-button=cancelDeleteForm]', function (e) {
        e.preventDefault();
        $(this).closest('form').arcticmodal('close');
    });

    $('body').on('submit', '#deleteCatForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'api/v1/category/' + $(this).find('[name=id]').val(),
            type: 'DELETE',
            dataType: 'json',
            success: function (response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $('#deleteCatForm')[0].reset();
                    $('#deleteCatForm').arcticmodal('close');
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('submit', '#deleteGoodForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'api/v1/good/' + $(this).find('[name=id]').val(),
            type: 'DELETE',
            dataType: 'json',
            success: function (response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $('#deleteGoodForm')[0].reset();
                    $('#deleteGoodForm').arcticmodal('close');
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('click', '#getCatByIdFormButton', function (e) {
        e.preventDefault();
        getCatById($('#getCatByIdForm input').val());
    });

    $('body').on('click', '#getGoodByIdFormButton', function (e) {
        e.preventDefault();
        getGoodById($('#getGoodByIdForm input').val());
    });

    $('body').on('submit', '#addCatForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'api/v1/category',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $('#addCatForm')[0].reset();
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('submit', '#addGoodForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'api/v1/good',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $('#addGoodForm')[0].reset();
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('submit', '#editCatForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'api/v1/category/' + $(this).find('[name=id]').val(),
            type: 'PUT',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $('#editCatForm')[0].reset();
                    $('#editCatForm').arcticmodal('close');
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });

    $('body').on('submit', '#editGoodForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'api/v1/good/' + $(this).find('[name=id]').val(),
            type: 'PUT',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (response) {
                if (response['success'] == 'true') {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: response['message']
                    });
                    $('#editGoodForm')[0].reset();
                    $('#editGoodForm').arcticmodal('close');
                } else {
                    new PNotify({
                        title: 'Ошибка',
                        type: 'error',
                        text: response['message']
                    })
                }
            }
        })
    });
});

function getCatById(id) {
    $.ajax({
        url: 'api/v1/category/' + id,
        type: 'GET',
        success: function (response) {
            response = JSON.parse(response);
            $('#getCatByIdFormContent tbody').html(renderCatTable(response));
        }
    })
}

function getGoodById(id) {
    $.ajax({
        url: 'api/v1/good/' + id,
        type: 'GET',
        success: function (response) {
            response = JSON.parse(response);
            $('#getGoodByIdFormContent tbody').html(renderGoodsTable(response));
        }
    })
}

function updateCats(start)
{
    $.ajax({
        url: 'api/v1/categories',
        type: 'GET',
        success: function(response) {
            try {
                response = JSON.parse(response);
                if (start != true) {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: 'Список категорий успешно обновлён'
                    })
                }
            } catch (e) {
                new PNotify({
                    title: 'Ошибка',
                    type: 'error',
                    text: 'Ошибка ' + e.name + ":" + e.message
                })
            }
            $('#categoriesTable tbody').html(renderCatTable(response));
        }
    });
}

function updateGoods(start) {
    $.ajax({
        url: 'api/v1/goods',
        type: 'GET',
        success: function(response) {
            try {
                response = JSON.parse(response);
                if (start != true) {
                    new PNotify({
                        title: 'Успех',
                        type: 'success',
                        text: 'Список товаров успешно обновлён'
                    })
                }
            } catch (e) {
                new PNotify({
                    title: 'Ошибка',
                    type: 'error',
                    text: 'Ошибка ' + e.name + ":" + e.message
                })
            }
            $('#goodsTable tbody').html(renderGoodsTable(response));
        }
    });
}
function renderCatTable(response)
{
    html = '';
    if (Object.prototype.toString.call( response ) === '[object Array]') {
        response.forEach(function (el, i) {
            html +=
                '<tr>' +
                    '<td>' + el.id + '</td>' +
                    '<td>' + el.name + '</td>' +
                    '<td>' + el.description + '</td>' +
                    '<td>' + el.created_at + '</td>' +
                    '<td>' + el.updated_at + '</td>' +
                    '<td>' +
                        '<button data-type="editCat" class="btn btn-success">Редактировать</button>' +
                        '<button data-type="deleteCat" class="btn btn-default">Удалить</button>' +
                    '</td>' +
                '</tr>';
        });
    } else {
        if (response['success'] == 'false') {
            new PNotify({
                title: 'Ошибка',
                type: 'error',
                text: response['message']
            });
        } else {
            html +=
                '<tr>' +
                '<td>' + response.id + '</td>' +
                '<td>' + response.name + '</td>' +
                '<td>' + response.description + '</td>' +
                '<td>' + response.created_at + '</td>' +
                '<td>' + response.updated_at + '</td>' +
                '<td>' +
                '<button data-type="editCat" class="btn btn-success">Редактировать</button>' +
                '<button data-type="deleteCat" class="btn btn-default">Удалить</button>' +
                '</td>' +
                '</tr>';
        }
    }
    return html;
}
function renderGoodsTable(response)
{
    html = '';
    if (Object.prototype.toString.call( response ) === '[object Array]') {
        response.forEach(function (el, i) {
            html +=
                '<tr>' +
                    '<td>' + el.id + '</td>' +
                    '<td>' + el.good_id + '</td>' +
                    '<td>' + el.name + '</td>' +
                    '<td>' + el.amount + '</td>' +
                    '<td>' + el.price + '</td>' +
                    '<td>' + el.options + '</td>' +
                    '<td>' + el.cat_id + '</td>' +
                    '<td>' + el.created_at + '</td>' +
                    '<td>' + el.updated_at + '</td>' +
                    '<td>' +
                    '<button data-type="editGood" class="btn btn-success">Редактировать</button>' +
                    '<button data-type="deleteGood" class="btn btn-default">Удалить</button>' +
                    '</td>' +
                '</tr>';
        });
    } else {
        if (response['success'] == 'false') {
            new PNotify({
                title: 'Ошибка',
                type: 'error',
                text: response['message']
            });
        } else {
            html +=
                '<tr>' +
                '<td>' + response.id + '</td>' +
                '<td>' + response.good_id + '</td>' +
                '<td>' + response.name + '</td>' +
                '<td>' + response.amount + '</td>' +
                '<td>' + response.price + '</td>' +
                '<td>' + response.options + '</td>' +
                '<td>' + response.cat_id + '</td>' +
                '<td>' + response.created_at + '</td>' +
                '<td>' + response.updated_at + '</td>' +
                '<td>' +
                '<button data-type="editGood" class="btn btn-success">Редактировать</button>' +
                '<button data-type="deleteGood" class="btn btn-default">Удалить</button>' +
                '</td>' +
                '</tr>';
        }
    }
    return html;
}