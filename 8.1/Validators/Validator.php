<?php
namespace Validators;

class Validator
{
    public static function filter($validateClass, $validation_rules = [], $filter_rules = [], $params = [])
    {
        if (!empty($params)) {
            $validateClass->validationRules($validation_rules);
            $validateClass->filterRules($filter_rules);

            $validated_data = $validateClass->run($params);

            return $validated_data;
        }
    }
}