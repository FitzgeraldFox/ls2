<?php
namespace Validators;


class ParseFormData
{
    public static function parseRawHttpRequest(array &$a_data = [])
    {
        // read incoming data
        $input = parse_url(urldecode(file_get_contents('php://input')))['path'];
        // grab multipart boundary from content type header
        preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
        if (empty($matches)) {
            $parts = explode('&', $input);
            $res_arr = [];
            foreach ($parts as $part) {
                $mini_parts = explode('=', $part);
                $res_arr[$mini_parts[0]] = $mini_parts[1];
            }
            return $res_arr;
        };
        $boundary = $matches[1];

        // split content by boundary and get rid of last -- element
        $a_blocks = preg_split("/-+$boundary/", $input);
        array_pop($a_blocks);

        // loop data blocks
        foreach ($a_blocks as $id => $block) {
            if (empty($block)) {
                continue;
            }
            preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
            $a_data[$matches[1]] = $matches[2];
        }
        return $a_data;
    }
}