<?php
namespace Validators;

use GUMP;

class ValidatorHandler
{
    public $validator;

    public function __construct($mode)
    {
        switch ($mode) {
            case 'gump':
                $this->validator = new GUMP;
                break;
        }
    }

    public function validationRules($params = [])
    {
        if (!empty($params)) {
            $this->validator->validation_rules($params);
        }
    }

    public function filterRules($params = [])
    {
        if (!empty($params)) {
            $this->validator->filter_rules($params);
        }
    }

    public function run($params = [])
    {
        if (!empty($params)) {
            $validated_data = $this->validator->run($params);
        }

        if (!$validated_data) {
            die(json_encode(['success' => 'false', 'message' => $this->validator->get_readable_errors(true)]));
        }

        return $validated_data;
    }
}