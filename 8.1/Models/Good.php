<?php
namespace Models;

require_once 'config.php';

use \Illuminate\Database\Capsule\Manager as Capsule;

class Good extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'goods';
}