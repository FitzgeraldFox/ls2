<?php

require_once 'config.php';
require "vendor/autoload.php";
use \Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('categories', function ($table) {
    $table->increments('id');
    $table->string('name', 100);
    $table->text('description');
    $table->timestamps();
});

Capsule::schema()->create('goods', function ($table) {
    $table->increments('id');
    $table->integer('good_id')->unique();
    $table->string('name', 100);
    $table->integer('amount')->unsigned();
    $table->integer('price')->unsigned();
    $table->json('options');
    $table->integer('cat_id')->unsigned();
    $table->foreign('cat_id')->references('id')->on('categories')->onDelete('CASCADE');
    $table->timestamps();
});
