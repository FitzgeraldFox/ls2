<?php
require_once 'config.php';
require "vendor/autoload.php";
use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->dropIfExists('goods');
Capsule::schema()->dropIfExists('categories');
